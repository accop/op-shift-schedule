"""
A common part for many test.
Elaborated schedule that contains rotations, many users and many slots.
Methods for making full schedule provided at hand, along dedicated methods for roles, slots, shitIds
"""

from members.models import Role
from shifts.models import *
import pytz


FUNNY_SHIFT_LEADER = "FunnyShiftLeader"
GRUMPY_SHIFT_LEADER = "GrumpyShiftLeader"
POOR_SHIFT_LEADER = "PoorShiftLeaderGuy"
SOME_POOR_OPERATOR = "SomePoorOperator"


def make_test_schedule(printPlan=False):
    """
    Elaborated schedule that contains rotations, many users and many slots.
    Methods for making full schedule provided at hand, along dedicated methods for roles, slots, shitIds

    --->> TEST SCHEDULE <<---
    FunnyShiftLeader 2022-04-30 Normal Working Hours (08:00:00 - 16:30:00)

    FunnyShiftLeader 2022-05-01 Morning (06:00:00 - 14:00:00)
    SomePoorOperator 2022-05-01 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-01 Afternoon (14:00:00 - 22:00:00)
    AnotherPoorOperator 2022-05-01 Afternoon (14:00:00 - 22:00:00)
    PoorShiftLeaderGuy 2022-05-01 Night (22:00:00 - 06:00:00)
    UnknownPoorOperator 2022-05-01 Night (22:00:00 - 06:00:00)
    OfficeHoursEmployee 2022-05-01 Normal Working Hours (08:00:00 - 16:30:00)

    FunnyShiftLeader 2022-05-03 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-03 Afternoon (14:00:00 - 22:00:00)

    OfficeHoursEmployee 2022-05-03 Normal Working Hours (08:00:00 - 16:30:00)
    OfficeHoursEmployee 2022-05-04 Normal Working Hours (08:00:00 - 16:30:00)

    FunnyShiftLeader 2022-05-10 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-10 Afternoon (14:00:00 - 22:00:00)
    PoorShiftLeaderGuy 2022-05-10 Night (22:00:00 - 06:00:00)
    OfficeHoursEmployee 2022-05-10 Normal Working Hours (08:00:00 - 16:30:00)

    FunnyShiftLeader 2022-05-11 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-11 Afternoon (14:00:00 - 22:00:00)
    PoorShiftLeaderGuy 2022-05-11 Night (22:00:00 - 06:00:00)
    OfficeHoursEmployee 2022-05-11 Normal Working Hours (08:00:00 - 16:30:00)

    FunnyShiftLeader 2022-05-12 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-12 Afternoon (14:00:00 - 22:00:00)
    PoorShiftLeaderGuy 2022-05-12 Night (22:00:00 - 06:00:00)
    OfficeHoursEmployee 2022-05-12 Normal Working Hours (08:00:00 - 16:30:00)

    GrumpyShiftLeader 2022-05-13 Morning (06:00:00 - 14:00:00)
    PoorShiftLeaderGuy 2022-05-13 Normal Working Hours (08:00:00 - 16:30:00)

    GrumpyShiftLeader 2022-05-14 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-15 Morning (06:00:00 - 14:00:00)
    GrumpyShiftLeader 2022-05-16 Morning (06:00:00 - 14:00:00)
    """
    testShifts, campaign, revision = setup_schedule()
    create_test_shifts(slotsMembersDates=testShifts, campaign=campaign, revision=revision)

    if printPlan:
        print("=========================")
        print("--->> TEST SCHEDULE <<---")
        for shift in Shift.objects.all():
            print(shift)
        print("=========================")


def setup_schedule():
    # Minimum object setup needed for the tests
    # example of a full 24h cycle
    # AM 7-15, PM 14-22, NG 22-06(day after)

    roleSL, roleOP = create_two_test_roles()
    member1 = create_test_member("Morning member", FUNNY_SHIFT_LEADER, roleSL)
    member2 = create_test_member("Afternoon member", GRUMPY_SHIFT_LEADER, roleSL)
    member3 = create_test_member("Night member", POOR_SHIFT_LEADER, roleSL)
    member1op = create_test_member("Morning OP", SOME_POOR_OPERATOR, roleOP)
    member2op = create_test_member("Afternoon OP", "AnotherPoorOperator", roleOP)
    member3op = create_test_member("Night OP", "UnknownPoorOperator", roleOP)
    memberOffice = create_test_member("Office Hours member", "OfficeHoursEmployee", roleSL)
    # prepare some simple schedule
    shiftID0, shiftID1, shiftID2 = create_test_shiftIDs()
    AM, PM, NG, NWH = create_four_test_slots()
    AM30, PM30, NG30, NWHx = create_four_test_slots(minutes=30)
    campaign, revision = create_test_campaign_revision()
    a10, b10, c10, a11, b11, c11, a12 = create_test_shiftIDs_multiple()
    c1, c2 = create_test_shiftIDs_crewTests()
    testShifts = (  # The TEST schedule
        # standalone correct NWH shift
        (member1, NWH, datetime.date(2022, 4, 30), shiftID0, False),
        # 24h coverage with NWH overlapping
        (member1, AM, datetime.date(2022, 5, 1), shiftID1, False),
        (member1op, AM, datetime.date(2022, 5, 1), shiftID1, False),
        (member2, PM, datetime.date(2022, 5, 1), shiftID2, False),
        (member2op, PM, datetime.date(2022, 5, 1), shiftID2, False),
        (member3, NG, datetime.date(2022, 5, 1), None, False),
        (member3op, NG, datetime.date(2022, 5, 1), None, False),
        (memberOffice, NWH, datetime.date(2022, 5, 1), None, False),
        # AM/PM with NWH overlapping
        (member1, AM, datetime.date(2022, 5, 3), None, False),
        (member2, PM, datetime.date(2022, 5, 3), None, False),
        (memberOffice, NWH, datetime.date(2022, 5, 3), None, False),
        # just NWH
        (memberOffice, NWH, datetime.date(2022, 5, 4), None, False),
        # more than one 24h round with some ppl scheduled NWH
        (member1, AM, datetime.date(2022, 5, 10), a10, False),
        (member2, PM, datetime.date(2022, 5, 10), b10, False),
        (member3, NG, datetime.date(2022, 5, 10), c10, False),
        (memberOffice, NWH, datetime.date(2022, 5, 10), None, False),
        (member1, AM, datetime.date(2022, 5, 11), a11, False),
        (member2, PM, datetime.date(2022, 5, 11), b11, False),
        (member3, NG, datetime.date(2022, 5, 11), c11, False),
        (memberOffice, NWH, datetime.date(2022, 5, 11), None, False),
        (member1, AM, datetime.date(2022, 5, 12), a12, False),
        (member2, PM, datetime.date(2022, 5, 12), None, False),
        (member3, NG, datetime.date(2022, 5, 12), None, False),
        (memberOffice, NWH, datetime.date(2022, 5, 12), None, False),
        (member2, AM, datetime.date(2022, 5, 13), None, False),
        (member3, NWH, datetime.date(2022, 5, 13), None, False),
        (member2, AM, datetime.date(2022, 5, 14), None, False),
        (member2, AM, datetime.date(2022, 5, 15), None, False),
        (member2, AM, datetime.date(2022, 5, 16), None, False),
        (member2, AM, datetime.date(2022, 5, 17), None, False),
        (member2, AM, datetime.date(2022, 5, 18), None, False),
        (member2, AM, datetime.date(2022, 5, 19), None, False),
        (member2, NG, datetime.date(2022, 5, 20), None, False),
        (member2, NWH, datetime.date(2022, 5, 27), None, False),
        (member3, NWH, datetime.date(2022, 6, 27), None, True),
        (member3, NG, datetime.date(2022, 6, 30), None, True),
        (member3, NG30, datetime.date(2024, 12, 7), c1, False),
        (member3op, NG30, datetime.date(2024, 12, 7), c1, False),
        (member3, NG30, datetime.date(2024, 12, 8), c2, False),
        (member3op, NG30, datetime.date(2024, 12, 8), c2, False),
        (member1, NG30, datetime.date(2024, 12, 9), None, False),
        # (member1op, NG, datetime.date(2022, 8, 3), None, False),
    )
    return testShifts, campaign, revision


def create_test_shifts(slotsMembersDates=None, campaign=None, revision=None):
    shiftRole = ShiftRole(name="OFFICE", abbreviation="OFF")
    shiftRole.save()
    for one in slotsMembersDates:
        shift = Shift()
        shift.member = one[0]  # member1
        shift.slot = one[1]  # AM
        if "NWH" in one[1].abbreviation:
            shift.role = shiftRole
        shift.campaign = campaign
        shift.revision = revision
        shift.date = one[2]  # datetime.date(2022, 5, 1)
        if one[3] is not None:
            shift.shiftID = one[3]
        if one[4]:
            shift.is_active = False
            shift.is_vacation = True
        shift.save()


def create_test_member(username, firstname, role):
    member1 = Member(username=username)
    member1.first_name = firstname
    member1.role = role
    member1.save()
    return member1


def create_test_campaign_revision():
    revision = Revision(date_start=datetime.datetime(2020, 12, 30, 12, 00, 00, tzinfo=pytz.UTC), valid=True)
    revision.save()
    campaign = Campaign(
        name="test",
        date_start=datetime.datetime(2020, 12, 31, 12, 00, 00, tzinfo=pytz.UTC),
        date_end=datetime.datetime(2020, 12, 31, 23, 00, 00, tzinfo=pytz.UTC),
    )
    campaign.save()
    return campaign, revision


def create_test_shiftIDs():
    shiftID0 = ShiftID(label="20220430A", date_created=datetime.datetime(2022, 4, 30, 7, 00, 00, tzinfo=pytz.UTC))
    shiftID0.save()
    shiftID1 = ShiftID(label="20220501A", date_created=datetime.datetime(2022, 5, 1, 7, 00, 00, tzinfo=pytz.UTC))
    shiftID1.save()
    shiftID2 = ShiftID(label="20220501B", date_created=datetime.datetime(2022, 5, 1, 14, 14, 00, tzinfo=pytz.UTC))
    shiftID2.save()
    return shiftID0, shiftID1, shiftID2


def create_test_shiftIDs_multiple():
    a10 = ShiftID(label="20220510A", date_created=datetime.datetime(2022, 5, 10, 7, 00, 00, tzinfo=pytz.UTC))
    a10.save()
    b10 = ShiftID(label="20220510B", date_created=datetime.datetime(2022, 5, 10, 14, 30, 00, tzinfo=pytz.UTC))
    b10.save()
    c10 = ShiftID(label="20220510C", date_created=datetime.datetime(2022, 5, 10, 22, 14, 00, tzinfo=pytz.UTC))
    c10.save()
    a11 = ShiftID(label="20220511A", date_created=datetime.datetime(2022, 5, 11, 7, 00, 00, tzinfo=pytz.UTC))
    a11.save()
    b11 = ShiftID(label="20220511B", date_created=datetime.datetime(2022, 5, 11, 14, 30, 00, tzinfo=pytz.UTC))
    b11.save()
    c11 = ShiftID(label="20220511C", date_created=datetime.datetime(2022, 5, 11, 22, 14, 00, tzinfo=pytz.UTC))
    c11.save()
    a12 = ShiftID(label="20220512A", date_created=datetime.datetime(2022, 5, 12, 7, 00, 00, tzinfo=pytz.UTC))
    a12.save()
    return (
        a10,
        b10,
        c10,
        a11,
        b11,
        c11,
        a12,
    )


def create_test_shiftIDs_crewTests():
    shiftID0 = ShiftID(label="20241207C", date_created=datetime.datetime(2024, 12, 7, 22, 45, 00, tzinfo=pytz.UTC))
    shiftID0.save()
    shiftID1 = ShiftID(label="20241208C", date_created=datetime.datetime(2024, 12, 8, 22, 45, 00, tzinfo=pytz.UTC))
    shiftID1.save()
    return (
        shiftID0,
        shiftID1,
    )


def create_two_test_roles():
    roleSL = Role(name="ShiftLeader", abbreviation="SL")
    roleSL.save()
    roleOP = Role(name="Operator", abbreviation="OP")
    roleOP.save()
    return roleSL, roleOP


def create_four_test_slots(minutes=0):
    abbrAM = "AM" if minutes == 0 else "AM" + str(minutes)
    try:
        AM = Slot.objects.get(abbreviation=abbrAM)
        AM.with_handover = True
        AM.save()
    except Slot.DoesNotExist:
        AM = Slot(name="Morning", hour_start=datetime.time(7, minutes, 0), hour_end=datetime.time(15, minutes, 0), abbreviation=abbrAM)
        AM.id_code = "A"
        AM.with_handover = True
        AM.save()
        # print(AM, AM.id_code)
    abbrPM = "PM" if minutes == 0 else "PM" + str(minutes)
    try:
        PM = Slot.objects.get(abbreviation=abbrPM)
        PM.with_handover = True
        PM.save()
    except Slot.DoesNotExist:
        # print("create pm")
        PM = Slot(name="Evening", hour_start=datetime.time(14, minutes, 0), hour_end=datetime.time(22, minutes, 00), abbreviation=abbrPM)
        PM.id_code = "B"
        PM.with_handover = True
        PM.save()
        # print(PM, PM.id_code)
    abbrNG = "NG" if minutes == 0 else "NG" + str(minutes)
    try:
        NG = Slot.objects.get(abbreviation=abbrNG)
        NG.with_handover = True
        NG.save()
    except Slot.DoesNotExist:
        # print("create night")
        NG = Slot(
            name="Night",
            hour_start=datetime.time(22, minutes, 0),
            hour_end=datetime.time(6, minutes, 0),
            abbreviation=abbrNG,
        )
        NG.id_code = "C"
        NG.with_handover = True
        NG.save()
        # print(NG, NG.id_code)
    abbrNWH = "NWH" if minutes == 0 else "NWH" + str(minutes)
    try:
        NWH = Slot.objects.get(abbreviation=abbrNWH)
    except Slot.DoesNotExist:
        NWH = Slot(name="NormalWH", hour_start=datetime.time(8, 0, 0), hour_end=datetime.time(16, 30, 0), abbreviation=abbrNWH)
        NWH.save()
    return AM, PM, NG, NWH
