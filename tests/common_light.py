"""
Simplified shift test environment, only few slots, base revision, campaign and one member.
Additionally, a method to create a shift is provided.
"""
from shifts.models import *


def get_shift(slot=None, fancy_date=None, username="test"):
    shift = Shift()
    shift.role = None
    shift.revision = Revision.objects.all()[0]
    shift.date = fancy_date
    shift.slot = slot
    shift.member = Member.objects.get(username=username)
    shift.csv_upload_tag = "TEST"
    shift.campaign = Campaign.objects.get(name="test")
    return shift


def setUpSlots():
    try:
        AM = Slot.objects.get(abbreviation="AM1")
        AM.with_handover = True
        AM.save()
        print("====>>>>", AM)
    except Slot.DoesNotExist:
        AM = Slot(name="Morning", hour_start=datetime.time(6, 30, 0), hour_end=datetime.time(14, 30, 0), abbreviation="AM1", id_code="A", with_handover=True)
        AM.save()
    try:
        PM = Slot.objects.get(abbreviation="PM1")
        PM.with_handover = True
        PM.save()
        print("====>>>>", PM)
    except Slot.DoesNotExist:
        PM = Slot(name="Evening", hour_start=datetime.time(14, 30, 0), hour_end=datetime.time(22, 30, 00), abbreviation="PM1", id_code="B", with_handover=True)
        PM.save()
    try:
        NG = Slot.objects.get(abbreviation="NG1")
        NG.with_handover = True
        NG.save()
        print("====>>>>", NG)
    except Slot.DoesNotExist:
        NG = Slot(name="Night", hour_start=datetime.time(22, 30, 0), hour_end=datetime.time(6, 30, 0), abbreviation="NG1", id_code="C", with_handover=True)
        NG.save()


def setUpRevisionsCampaignsMembers():
    revision = Revision(date_start=datetime.datetime(2020, 12, 30, 12, 00, 00), valid=True)
    revision.save()
    campaign = Campaign(name="test")
    campaign.date_start = datetime.datetime(2020, 12, 31, 12, 00, 00)
    campaign.date_end = datetime.datetime(2020, 12, 31, 23, 00, 00)
    campaign.save()
    member = Member(username="test", first_name="Some Fancy First Name")
    member.save()
