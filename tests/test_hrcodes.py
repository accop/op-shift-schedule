from .common_light import setUpSlots, setUpRevisionsCampaignsMembers, get_shift
from django.test import TestCase
from shifts.models import Slot, SIMPLE_DATE
import shifts.hrcodes as hrc
from shifts.hrcodes import OB1, OB2, OB3, OB4, NWH, LC1, LC2, ELC, BTP_OR_BTB, OTT_OR_OTW
import datetime

# # test setup for slots full hours, no handover
HOURS_AM_00 = {OB1: 0, OB2: 1, OB3: 0, OB4: 0, NWH: 7}  #
HOURS_PM_00 = {OB1: 4, OB2: 0, OB3: 0, OB4: 0, NWH: 4}  #
# HOURS_NWH_BH = {OB1: 0, OB2: 0, OB3: 0, OB4: 4, NWH: 4}  #
HOURS_NG_00 = {OB1: 2, OB2: 6, OB3: 0, OB4: 0, NWH: 0}  #
HOURS_WE_00 = {OB1: 0, OB2: 0, OB3: 8, OB4: 0, NWH: 0}  # + normal holidays, like WE
HOURS_BH_00 = {OB1: 0, OB2: 0, OB3: 0, OB4: 8, NWH: 0}  # special holidays
HOURS_NWH_00 = {OB1: 0, OB2: 0, OB3: 0, OB4: 0, NWH: 7.8}  # special holidays
# HOURS_REDUCED = {OB1: 0, OB2: 0, OB3: 0, OB4: 0, NWH: 5}  # NWH during reduced hour day
AM_00 = "AM"
PM_00 = "PM"
NG_00 = "NG"
NWH_00 = NWH

# new setup for 6:30-14:30-22:30-6:30, AND each of those slots with extra 0.25 (defined in the slot)
HOURS_AM_30 = {OB1: 0, OB2: 0.5, OB3: 0, OB4: 0, NWH: 7.75}  #
HOURS_PM_30 = {OB1: 4.75, OB2: 0, OB3: 0, OB4: 0, NWH: 3.5}  #
HOURS_NG_30 = {OB1: 1.5, OB2: 6.75, OB3: 0, OB4: 0, NWH: 0}  #
HOURS_WE_30 = {OB1: 0, OB2: 0, OB3: 8.25, OB4: 0, NWH: 0}  # + normal holidays, like WE
HOURS_WE_OFFICE_30 = {OB1: 0, OB2: 0, OB3: 7.8, OB4: 0, NWH: 0}  # + normal holidays, like WE with the OFFICE day scheduled
HOURS_WE_30_NIGHT_WITH_NEXT_DAY_NORMAL = {OB1: 0, OB2: 6.75, OB3: 1.5, OB4: 0, NWH: 0}  # SundayWE
HOURS_BH_30 = {OB1: 0, OB2: 0, OB3: 0, OB4: 8.25, NWH: 0}  # special holidays
HOURS_BH_30_NIGHT_WITH_NEXT_DAY_NORMAL = {OB1: 0, OB2: 6.75, OB3: 0, OB4: 1.5, NWH: 0}  # special holidays
AM_30 = "AM1"
PM_30 = "PM1"
NG_30 = "NG1"


class HRCodesSingleShift(TestCase):
    def setUp(self):
        setUpSlots()
        setUpRevisionsCampaignsMembers()

    def test_office_day(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2024, 9, 18))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_NWH_00)

    def test_office_day_on_red_day(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2025, 1, 6))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_OFFICE_30)

    def test_office_day_on_other_red_day(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2025, 1, 5))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_OFFICE_30)

    def test_codes_simple(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2021, 8, 26))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_AM_30)

    def test_codes_simple_00(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_00), fancy_date=datetime.date(2021, 8, 26))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_AM_00)

    def test_codes_incl_late_evening(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2021, 8, 23))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_PM_30)

    def test_codes_incl_late_evening_00(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_00), fancy_date=datetime.date(2021, 8, 23))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_PM_00)

    def test_codes_overnight(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_30), fancy_date=datetime.date(2021, 8, 26))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_NG_30)

    def test_codes_overnight_00(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_00), fancy_date=datetime.date(2021, 8, 26))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_NG_00)

    def test_codes_WE(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2021, 8, 28))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_WE_SUNDAY_DAY(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2025, 1, 12))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_WE_SUNDAY_NIGHT(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_30), fancy_date=datetime.date(2025, 1, 12))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30_NIGHT_WITH_NEXT_DAY_NORMAL)

    def test_codes_WE_SUNDAY_AFTERNOON(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2025, 1, 26))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_WE_SUNDAY_AFTERNOON_with_Mon_RED(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2025, 1, 5))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_WE_SATURDAY_AFTERNOON_with_Mon_RED(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2025, 1, 4))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_EASTER_MONDAY_AFTERNOON(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2025, 4, 21))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_30)

    def test_codes_EASTER_MONDAY_NIGHT(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_30), fancy_date=datetime.date(2025, 4, 21))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_30_NIGHT_WITH_NEXT_DAY_NORMAL)

    def test_codes_WE_00(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_00), fancy_date=datetime.date(2021, 8, 28))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_00)

    def test_codes_BankHoliday(self):  # on bank holiday
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2021, 6, 25))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_30)

    def test_codes_BankHoliday_00(self):  # on bank holiday
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_00), fancy_date=datetime.date(2021, 6, 25))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_00)

    def test_codes_BankHoliday_on_WE(self):  # on bank holiday over WE
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2023, 4, 9))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_30)

    def test_codes_BankHoliday_on_WE_00(self):  # on bank holiday over WE
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_00), fancy_date=datetime.date(2023, 4, 9))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_00)

    def test_codes_BankHoliday_AdjWE(self):  # a WE after bank holiday
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2021, 6, 25))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_30)

    def test_codes_Not_BankHoliday(self):  # a shift a day AFTER  bank holiday
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2022, 4, 19))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_AM_30)

    def test_codes_Not_BankHoliday2(self):  # a two shift two day AFTER bank holiday
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2022, 4, 20))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_AM_30)

    def test_codes_BankHolidayXmasEve(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2022, 12, 24))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_BH_30)

    # SUPER DUPER SPECIAL CASE, that for now is not covered :-D
    # def test_codes_Maundy_Thursday_2023_AM(self):
    #     shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2023, 4, 6))
    #     counts, countsCorrection = hrc.get_code_counts(shift)
    #     self.compare(counts, HOURS_AM_30)
    #
    # def test_codes_Maundy_Thursday_holiday_2022_PM(self):
    #     shift = get_shift(slot=Slot.objects.get(abbreviation=PM), fancy_date=datetime.date(2023, 4, 6))
    #     self.compare(hrc.get_code_counts(shift), HOURS_NWH_BH)
    #
    # def test_codes_Maundy_Thursday_holiday_2023_NG(self):
    #     shift = get_shift(slot=Slot.objects.get(abbreviation=NG), fancy_date=datetime.date(2023, 4, 6))
    #     self.compare(hrc.get_code_counts(shift), HOURS_BH)

    def test_codes_red_day_2023(self):  # a shift on a red day -> OB3
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2023, 5, 1))
        counts, countsCorrection = hrc.get_code_counts(shift)
        self.compare(counts, HOURS_WE_30)

        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_30), fancy_date=datetime.date(2023, 5, 1))
        counts, countsCorrection = hrc.get_code_counts(shift)
        self.compare(counts, HOURS_WE_30_NIGHT_WITH_NEXT_DAY_NORMAL)

    def test_codes_red_day_2022(self):  # a shift on a red day -> OB3
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2022, 6, 6))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_red_day_2025(self):  # a shift on a red day -> OB3
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2025, 1, 6))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30)

    def test_codes_red_day_2025_Monday_night(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_30), fancy_date=datetime.date(2025, 1, 6))
        h, hCorrection = hrc.get_code_counts(shift)
        self.compare(h, HOURS_WE_30_NIGHT_WITH_NEXT_DAY_NORMAL)

    def test_reduced_day_morning(self):  # a shift on a reduced day - no weird OB
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2023, 1, 5))
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(7.75, h.get(NWH))
        self.assertEqual(0.5, h.get(OB2))
        self.assertEqual(3.25, h.get(BTP_OR_BTB))

    def test_reduced_day_nwh(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2023, 1, 5))
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(7.8, h.get(NWH))
        self.assertEqual(3.0, h.get(BTP_OR_BTB))

    def test_reduced_day_afternoon(self):  # a pm shift on a reduced day still regular PM
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2023, 1, 5))
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(3.5, h.get(NWH))
        self.assertEqual(4.75, h.get(OB1))
        self.assertEqual(3.25, h.get(BTP_OR_BTB))

    def test_reduced_day_with_night_shift(self):  # a shift on a reduced day still regular NG
        shift = get_shift(slot=Slot.objects.get(abbreviation=NG_30), fancy_date=datetime.date(2023, 1, 5))
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(1.5, h.get(OB1))
        self.assertEqual(6.75, h.get(OB2))
        self.assertEqual(3.25, h.get(BTP_OR_BTB))

    def test_bridge_day_morning(self):  # a shift on a reduced day - no weird OB
        shift = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2024, 12, 27))
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(7.75, h.get(NWH))
        self.assertEqual(0.5, h.get(OB2))
        self.assertEqual(8.25, h.get(BTP_OR_BTB))

    def test_bridge_day_nwh(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2024, 12, 27))
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(7.8, h.get(NWH))
        self.assertEqual(8.0, h.get(BTP_OR_BTB))

    def test_codes_with_cancel_and_correction_shift_to_office(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2021, 8, 26))
        dateReport = datetime.date(2021, 8, 18)
        strftime = dateReport.strftime(SIMPLE_DATE)
        shift.changed_on_date = dateReport
        shift.is_changed = True
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertIsNotNone(hCorrection.get(strftime, None))
        self.assertEqual(1, h.get(LC1))
        self.assertEqual(1, hCorrection.get(strftime).get(ELC))
        self.assertEqual(0, h.get(OB1))
        self.assertEqual(0, h.get(OB2))
        self.assertEqual(0, h.get(OB3))
        self.assertEqual(0, h.get(OB4))
        self.assertEqual(7.8, h.get(NWH))

    def test_codes_with_cancel_and_correction_free_day_to_shift(self):
        shift = get_shift(slot=Slot.objects.get(abbreviation=PM_30), fancy_date=datetime.date(2021, 8, 28))
        dateReport = datetime.date(2021, 8, 27)
        strftime = dateReport.strftime(SIMPLE_DATE)
        shift.changed_on_date = dateReport
        shift.is_changed_urgent = True
        shift.is_changed_offday = True
        h, hCorrection = hrc.get_code_counts(shift)
        self.assertEqual(0, h.get(LC1))
        self.assertEqual(1, h.get(LC2))
        self.assertEqual(1, hCorrection.get(strftime).get(ELC))
        self.assertEqual(0, h.get(OB1))
        self.assertEqual(0, h.get(OB2))
        self.assertEqual(0, h.get(OB3))
        self.assertEqual(0, h.get(OB4))
        self.assertEqual(8.25, h.get(OTT_OR_OTW))

    def compare(self, codes1, codes2):
        for oneCode in codes1.keys():
            self.assertEqual(codes1.get(oneCode, 0), codes2.get(oneCode, 0))


class HRCodesManyShifts(TestCase):
    def setUp(self):
        setUpSlots()
        setUpRevisionsCampaignsMembers()

    def test_codes_with_shift_change(self):
        shift1 = get_shift(slot=Slot.objects.get(abbreviation=AM_30), fancy_date=datetime.date(2021, 8, 24))
        shift2 = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2021, 8, 26))
        dateReport = datetime.date(2021, 8, 24)
        strftime = dateReport.strftime(SIMPLE_DATE)
        shift2.changed_on_date = dateReport
        shift2.is_changed = True
        shift3 = get_shift(slot=Slot.objects.get(abbreviation=NWH_00), fancy_date=datetime.date(2021, 8, 27))
        dateReport = datetime.date(2021, 8, 24)
        strftime = dateReport.strftime(SIMPLE_DATE)
        shift3.changed_on_date = dateReport
        shift3.is_changed = True

        allShiftHours = hrc.get_date_code_counts([shift1, shift2, shift3])
        self.assertEqual(1, allShiftHours.get(strftime).get(ELC))
        self.assertEqual(1, allShiftHours.get(shift2.date.strftime(SIMPLE_DATE)).get(LC1))
        self.assertEqual(1, allShiftHours.get(shift3.date.strftime(SIMPLE_DATE)).get(LC1))
