from django.test import TestCase
from .common import *
from shifts.workinghours import find_weekly_rest_time_violation, find_daily_rest_time_violation
from shifts.workinghours import find_working_hours, get_working_hours


class ActiveShift(TestCase):
    def setUp(self):
        testShifts, self.campaign, self.revision = setup_schedule()
        create_test_shifts(slotsMembersDates=testShifts, campaign=self.campaign, revision=self.revision)

    def test_CheckMinimumTimeDaily_CORRECT(self):
        scheduled_shits = Shift.objects.filter(member__first_name=FUNNY_SHIFT_LEADER)
        found = find_daily_rest_time_violation(scheduled_shits)
        self.assertEqual(0, len(found))

    def test_CheckMinimumTimeDaily_WRONG(self):
        scheduled_shits = Shift.objects.filter(member__first_name=GRUMPY_SHIFT_LEADER)
        found = find_daily_rest_time_violation(scheduled_shits)
        self.assertEqual(1, len(found))

    def test_CheckMinimumTimeDaily_CORRECT_with_NWH(self):
        scheduled_shits = Shift.objects.filter(member__first_name=POOR_SHIFT_LEADER)
        found = find_daily_rest_time_violation(scheduled_shits)
        self.assertEqual(0, len(found))

    def test_CheckMinimumTimeWeekly_CORRECT(self):
        scheduled_shits = Shift.objects.filter(member__first_name=FUNNY_SHIFT_LEADER)
        found = find_weekly_rest_time_violation(scheduled_shits)
        self.assertEqual(0, len(found))

    def test_CheckMinimumTimeWeekly_WRONG(self):
        scheduled_shits = Shift.objects.filter(member__first_name=GRUMPY_SHIFT_LEADER)
        found = find_weekly_rest_time_violation(scheduled_shits)
        self.assertEqual(1, len(found))


class WorkingTime(TestCase):
    def setUp(self):
        testShifts, self.campaign, self.revision = setup_schedule()
        create_test_shifts(slotsMembersDates=testShifts, campaign=self.campaign, revision=self.revision)

    def test_get_shiftSlots(self):
        scheduled_shits = Shift.objects.filter(member__role__abbreviation="SL")
        find_working_hours(scheduled_shits)
        # print(a)


class YearlyWorkingTimeInHours(TestCase):
    def setUp(self):
        testShifts, self.campaign, self.revision = setup_schedule()
        create_test_shifts(slotsMembersDates=testShifts, campaign=self.campaign, revision=self.revision)
        default_start = datetime.datetime.strptime("2022-05-01", SIMPLE_DATE).date()
        default_end = default_start + datetime.timedelta(days=365)
        self.filterDict = {"date__gte": default_start, "date__lte": default_end, "revision": self.revision}

    def test_calculateGrumpyMember(self):
        # member 2, several different shifts
        member = Member.objects.get(first_name=GRUMPY_SHIFT_LEADER)
        r = get_working_hours(self.filterDict, [member])[0]
        self.assertEqual(r.get("total_working_hours_NWH"), 7.8)
        self.assertEqual(r.get("total_working_hours"), 107.25)

    def test_calculateOther(self):
        # member1op, only one shift
        member = Member.objects.get(first_name=SOME_POOR_OPERATOR)
        r = get_working_hours(self.filterDict, [member])[0]
        self.assertEqual(r.get("total_working_hours_NWH"), 0)
        self.assertEqual(r.get("total_working_hours"), 8.25)

    def test_calculateOtherMemberWithVacation(self):
        # member3, some shifts and some vacations
        member = Member.objects.get(first_name=POOR_SHIFT_LEADER)
        r = get_working_hours(self.filterDict, [member])[0]
        self.assertEqual(r.get("total_working_hours_NWH"), 15.8)
        self.assertEqual(r.get("total_working_hours"), 33)
