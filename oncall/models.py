from django.db.models import DO_NOTHING
from django.db import models
from django.utils import timezone
from members.models import Member


class SupportFunction(models.Model):
    TYPE_CHOICES = [
        ("ONCALL", "OnCall"),
        ("BETS", "BETS"),
        ("OTHER", "Other"),
    ]

    DOMAIN_CHOICES = [
        ("INFRA", "INFRA"),
        ("PSSGDS", "PSS GDS"),
        ("VACUUM", "Vacuum"),
        ("CRYO", "Cryo"),
        ("IOM_MECH", "IOM Mechanical"),
        ("IOM_ELEC", "IOM Electrical"),
        ("LINAC", "Linac"),
        ("RP", "RP"),
        ("RF", "RF"),
        ("POWER_CONV", "Power Converters"),
        ("BEAM_DIAG", "Beam Diagnostics"),
        ("OTHER", "Other"),
    ]

    type = models.CharField(max_length=20, choices=TYPE_CHOICES, default="ONCALL", verbose_name="Type")
    domain = models.CharField(max_length=50, choices=DOMAIN_CHOICES, verbose_name="Domain")
    phone_number = models.CharField(max_length=30, verbose_name="Phone Number", null=True, blank=True)
    manager_email = models.EmailField(max_length=254, verbose_name="Manager Email Address", null=True, blank=True)

    def __str__(self):
        return f"{self.get_type_display()} | {self.get_domain_display()} ({self.phone_number})"


class OnCallContact(models.Model):
    CALL_TYPE_CHOICES = [
        ("ONSITE", "On-Site"),
        ("REMOTE", "Remote"),
    ]

    STATUS_CHOICES = [
        ("RESOLVED", "Resolved"),
        ("POSTPONED", "Postponed"),
        ("ONGOING", "Ongoing"),
    ]

    support_function = models.ForeignKey(SupportFunction, on_delete=DO_NOTHING, blank=False, null=False)
    call_by = models.ForeignKey(
        Member,
        on_delete=DO_NOTHING,
        blank=False,
        null=False,
        related_name="calls_initiated",
    )
    call_time = models.DateTimeField(default=timezone.now, verbose_name="Call Time", blank=False, null=False)
    call_to = models.CharField(max_length=150, blank=True, verbose_name="Name of the person called")
    duration = models.DurationField(null=True, blank=True, verbose_name="Duration of the intervention [hh:mm:ss]")
    description = models.TextField(blank=True, verbose_name="Call/Intervention Description (free text)")
    summary_log = models.TextField(blank=True, verbose_name="Call/Intervention Summary Log (free text)")
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default="ONGOING", verbose_name="Intervention status")
    call_type = models.CharField(max_length=10, choices=CALL_TYPE_CHOICES, default="ONSITE", verbose_name="Intervention type")
    updated_by = models.ForeignKey(
        Member,
        on_delete=DO_NOTHING,
        null=True,
        blank=True,
        verbose_name="Last updated by",
        related_name="calls_updated",
    )

    def __str__(self):
        return f"Call #{self.pk} ({self.get_call_type_display()})"
