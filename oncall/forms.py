from django import forms
from .models import OnCallContact, SupportFunction
from members.models import Member


class OnCallContactForm(forms.ModelForm):
    class Meta:
        model = OnCallContact
        fields = [
            "call_by",
            "call_time",
            "support_function",
            "call_to",
            "duration",
            "call_type",
            "status",
            "description",
            "summary_log",
        ]
        widgets = {
            "call_time": forms.DateTimeInput(attrs={"type": "datetime-local", "class": "form-control"}),
            "description": forms.Textarea(attrs={"class": "form-control", "rows": 3}),
            "summary_log": forms.Textarea(attrs={"class": "form-control", "rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["status"].widget = forms.Select(choices=self.fields["status"].choices, attrs={"class": "form-select"})
        self.fields["call_type"].widget = forms.Select(choices=self.fields["call_type"].choices, attrs={"class": "form-select"})
        self.fields["support_function"].widget = forms.Select(
            attrs={"class": "form-select"}, choices=[(obj.id, f"{obj.type} - {obj.get_domain_display()}") for obj in SupportFunction.objects.all()]
        )
        self.fields["call_by"].widget = forms.Select(
            attrs={"class": "form-select"}, choices=[(obj.id, f"{obj.__str__()}") for obj in Member.objects.order_by("first_name")]
        )
        self.fields["call_to"].widget.attrs.update({"class": "form-control"})
        self.fields["duration"].widget.attrs.update({"class": "form-control"})
