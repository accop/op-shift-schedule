from django.contrib import admin
from .models import SupportFunction, OnCallContact


@admin.register(SupportFunction)
class SupportFunctionAdmin(admin.ModelAdmin):
    list_display = ("id", "type", "domain", "phone_number", "manager_email")
    search_fields = ("phone_number", "domain")
    list_filter = ("type", "domain")
    list_per_page = 20


@admin.register(OnCallContact)
class OnCallContactAdmin(admin.ModelAdmin):
    list_display = ("id", "call_time", "support_function", "call_by", "call_to", "duration", "status", "call_type")
    search_fields = ("call_to", "description", "summary_log")
    list_filter = ("status", "call_type", "support_function")
    date_hierarchy = "call_time"
    list_per_page = 20
