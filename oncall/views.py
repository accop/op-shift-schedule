from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Sum
from django.db.models.functions import ExtractHour, TruncDate
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_http_methods

from .models import OnCallContact, SupportFunction
from .forms import OnCallContactForm
from members.models import Member
from shifts.models import SIMPLE_DATE

from datetime import datetime, timedelta
from pytz import timezone
import numpy as np

DOMAIN_DISPLAY = {k: v for k, v in SupportFunction.DOMAIN_CHOICES}

VINTER_20_PALETTE = [
    "#1E3A8A",  # Deep Winter Blue
    "#93C5FD",  # Icy Sky
    "#BEE3F8",  # Frost Blue
    "#60A5FA",  # Glacier Blue
    "#2563EB",  # Cold Steel
    "#A7F3D0",  # Frozen Mint
    "#10B981",  # Evergreen
    "#64748B",  # Winter Fog
    "#334155",  # Midnight Sky
    "#CBD5E1",  # Soft Snow
    "#1E40AF",  # Polar Night Blue
    "#DBEAFE",  # Thin Ice
    "#D1FAE5",  # Pale Mint Frost
    "#0369A1",  # Deep Ice Current
    "#0E7490",  # Arctic Teal
    "#94A3B8",  # Overcast Grey
    "#F8FAFC",  # Snow Drift
    "#E0F2FE",  # Cold Sky Reflection
    "#D8B4FE",  # Frosted Lavender(a gentle touch of color)
    "#F1F5F9",  # Pure Ice
]


@login_required
def get_domain_colors(request):
    domains = [domain[1] for domain in SupportFunction.DOMAIN_CHOICES]
    domain_colors = {}
    for idx, domain in enumerate(domains):
        color_index = idx % len(VINTER_20_PALETTE)  # Cycle if more than 20
        domain_colors[domain] = VINTER_20_PALETTE[color_index]
    return JsonResponse(domain_colors)


@login_required
def oncall_log_view(request):
    default_filter = _get_filter(request)
    contacts = OnCallContact.objects.filter(**default_filter).order_by("-call_time")
    startDate = timezone("Europe/Stockholm").localize(datetime.now())
    endDate = startDate
    if len(contacts) > 1:
        endDate = contacts[0].call_time
        startDate = contacts[len(contacts) - 1].call_time
    support_types = {"All": ""}
    for x in SupportFunction.TYPE_CHOICES:
        support_types[x[1]] = x[0].upper()
    print(support_types)
    context = {"contacts": contacts, "default_start": startDate, "default_end": endDate, "support_types": support_types}
    return render(request, "oncall_log/oncall_log_list.html", context)


@login_required
def oncall_detail(request, id):
    oncall = get_object_or_404(OnCallContact, id=id)
    print(oncall.updated_by)
    context = {"oncall": oncall}
    return render(request, "oncall_log/oncall_log_detail.html", context)


@login_required
def call_create_view(request):
    if request.method == "POST":
        form = OnCallContactForm(request.POST)
        if form.is_valid():
            contact = form.save(commit=False)
            contact.called_by = request.user
            contact.update_by = request.user
            contact.save()
            return redirect("oncall:oncall_log")
    else:
        form = OnCallContactForm()
    return render(request, "oncall_log/oncall_log_create.html", {"form": form})


# should remain public for the time being
def oncall_log_contacts_json(request) -> JsonResponse:
    return JsonResponse(
        [
            {
                "id": c.id,
                "call_by": str(c.call_by),
                "updated_by": str(c.updated_by),
                "call_time": c.call_time.strftime("%Y-%m-%d %H:%M:%S"),
                "support_function": f"{c.support_function.get_type_display()} / {c.support_function.get_domain_display()}",
                "call_to": c.call_to,
                "duration": str(c.duration) if c.duration else "",
                "description": c.description,
                "summary_log": c.summary_log,
                "call_type": c.get_call_type_display(),
                "status": c.get_status_display(),
            }
            for c in (OnCallContact.objects.select_related("support_function", "call_by").filter(**_get_filter(request)).order_by("-call_time"))
        ],
        safe=False,
    )


@login_required
def oncall_update_view(request, id):
    oncall = get_object_or_404(OnCallContact, id=id)
    if request.method == "POST":
        form = OnCallContactForm(request.POST, instance=oncall)
        if form.is_valid():
            updated_oncall = form.save(commit=False)
            updated_oncall.updated_by = request.user
            updated_oncall.save()
            return redirect("oncall:oncall_log")
    else:
        form = OnCallContactForm(instance=oncall)
    return render(request, "oncall_log/oncall_log_edit.html", {"form": form, "oncall": oncall})


@login_required
def call_stat_by_function(request) -> JsonResponse:
    to_return = {"count": {}, "duration_min": {}}
    default_filter = _get_filter(request)
    for one in SupportFunction.DOMAIN_CHOICES:
        to_return["count"][DOMAIN_DISPLAY.get(one[0])] = OnCallContact.objects.filter(**default_filter).filter(support_function__domain=one[0]).count()
        to_return["duration_min"][DOMAIN_DISPLAY.get(one[0])] = (
            sum([x.duration.total_seconds() for x in OnCallContact.objects.filter(**default_filter).filter(support_function__domain=one[0])]) / 60
        )
    count_data = [{"name": key, "y": value} for key, value in to_return["count"].items()]
    duration_data = [{"name": key, "y": value} for key, value in to_return["duration_min"].items()]
    return JsonResponse({"count": count_data, "duration": duration_data, "title": _get_label_for_timerange(request)})


@login_required
def call_duration_histogram(request) -> HttpResponse:
    durations = [
        duration.total_seconds() / 60
        for duration in OnCallContact.objects.filter(**_get_filter(request)).values_list("duration", flat=True)
        if duration is not None  # Filter out None values
    ]
    if not durations:
        JsonResponse({"histogram": [], "title": _get_label_for_timerange(request)})
    bin_edges = np.histogram_bin_edges(durations, bins=10)
    histogram, _ = np.histogram(durations, bins=bin_edges)
    data = [{"name": f"{int(bin_edges[i])}-{int(bin_edges[i+1])}", "y": int(histogram[i])} for i in range(len(histogram))]
    return JsonResponse({"histogram": data, "title": _get_label_for_timerange(request)})


@login_required
def call_time_of_day_histogram(request) -> JsonResponse:
    time_data = (
        OnCallContact.objects.filter(**_get_filter(request))
        .annotate(hour=ExtractHour("call_time"))
        .values("hour")
        .annotate(count=Count("id"), total_duration=Sum("duration"))
        .order_by("hour")
    )
    categories = [f"{entry['hour']}:00-{entry['hour'] + 1}:00" for entry in time_data]
    count_series = [entry["count"] for entry in time_data]
    duration_series = [entry["total_duration"].total_seconds() / 60 if entry["total_duration"] else 0 for entry in time_data]
    return JsonResponse(
        {"categories": categories, "count_series": count_series, "duration_series": duration_series, "title": _get_label_for_timerange(request)}
    )


@login_required()
def call_daily_cumulative(request) -> JsonResponse:
    daily_durations = (
        OnCallContact.objects.filter(**_get_filter(request))
        .annotate(date=TruncDate("call_time"))
        .values("date", "support_function")
        .annotate(total_duration=Sum("duration"))
        .order_by("date")
    )
    cumulative_sum = 0
    timeline_data = {}
    domain_totals = {}
    for entry in daily_durations:
        date_str = entry["date"].strftime(SIMPLE_DATE)
        daily_minutes = entry["total_duration"].total_seconds() / 60 if entry["total_duration"] else 0
        support_function_code = entry["support_function"]
        domain_label = DOMAIN_DISPLAY.get(SupportFunction.objects.get(id=support_function_code).domain)
        cumulative_sum += daily_minutes / 60
        if date_str not in timeline_data:
            timeline_data[date_str] = {"cumulative_duration": cumulative_sum, "domains": {}}
        if domain_label not in timeline_data[date_str]["domains"]:
            timeline_data[date_str]["domains"][domain_label] = 0
        timeline_data[date_str]["domains"][domain_label] += daily_minutes
        domain_totals[domain_label] = domain_totals.get(domain_label, 0) + daily_minutes
    categories = sorted(timeline_data.keys())
    cumulative_series = [timeline_data[date]["cumulative_duration"] for date in categories]

    domain_series = {domain: [] for domain in domain_totals}
    for date in categories:
        for domain in domain_totals:
            domain_series[domain].append(timeline_data[date]["domains"].get(domain, 0))

    return JsonResponse(
        {"categories": categories, "cumulative_series": cumulative_series, "domain_series": domain_series, "title": _get_label_for_timerange(request)}
    )


@require_http_methods(["POST"])
@csrf_protect
@login_required
def call_upload_list_post(request):
    csv_file = request.FILES["csv_file"]
    if not csv_file.name.endswith(".csv"):
        messages.error(request, "Wrong file type! Needs to be CSV")
        return HttpResponseRedirect(reverse("oncall:oncall_log"))
    if csv_file.multiple_chunks():
        messages.error(request, "Too large file")
        return HttpResponseRedirect(reverse("oncall:oncall_log"))

    import csv

    decoded_file = csv_file.read().decode("utf-8").splitlines()
    reader = csv.reader(decoded_file)
    goodImports = 0
    for lineIndex, fields in enumerate(reader):
        if lineIndex == 0:
            continue

        proper_time = _get_proper_time(fields[0], fields[1])
        member = _get_member(fields[3])
        function = _get_support_function(fields[6], fields[5])
        callDuration = timedelta(minutes=int(fields[8]))
        callReason = fields[9]
        callSummaryLog = fields[11]
        callStatus = _get_status(fields[10])
        callType = _get_type(fields[10])

        if member is None or ("Other" in function.domain and "Other" in function.type):
            print("====== START ======")
            print(fields)
            print(proper_time)
            print(member)
            print(function)
            print(callStatus, callType)
            print("======= END =====")
        else:
            OnCallContact.objects.create(
                support_function=function,
                call_by=member,
                call_time=proper_time,
                call_to=fields[4],
                duration=callDuration,
                description=callReason,
                summary_log=callSummaryLog,
                status=callStatus,
                call_type=callType,
            )
            goodImports += 1
    messages.info(request, "File uploaded successfully. {} out of {} imported.".format(goodImports, lineIndex))
    return HttpResponseRedirect(reverse("oncall:oncall_log"))


# FIXME maybe move these to a helper module


def _get_filter(request):
    dateStart, dateEnd = _get_dates_range_from_request(request)
    filter_dict = {}
    if dateStart and dateEnd:
        filter_dict["call_time__date__range"] = [dateStart, dateEnd]

    if request.GET.get("call_type", None):
        filter_dict["support_function__type"] = request.GET.get("call_type")
    return filter_dict


def _get_support_function(domain, type):
    type_dict = {label: value for value, label in SupportFunction.TYPE_CHOICES}
    domain_dict = {label: value for value, label in SupportFunction.DOMAIN_CHOICES}
    domainToUse = domain  # .replace(" ", "")
    typeToUse = "OnCall" if "call" in type.lower() else "BETS"

    return (
        SupportFunction.objects.get(domain=domain_dict.get(domainToUse), type=type_dict.get(typeToUse))
        if (SupportFunction.objects.filter(domain=domain_dict.get(domainToUse), type=type_dict.get(typeToUse)).exists())
        else (SupportFunction.objects.get(domain="OTHER", type="OTHER"))
    )


def _get_dates_range_from_request(request):
    start_date = request.GET.get("start_date", None)
    end_date = request.GET.get("end_date", None)
    if start_date and end_date:
        return start_date, end_date
    return None, None


def _get_label_for_timerange(request):
    startDate, endDate = _get_dates_range_from_request(request)
    call_type = request.GET.get("call_type", "All")
    if len(call_type) == 0:
        call_type = "All"
    if startDate and endDate:
        return f"{startDate} to {endDate}, type: {call_type}"
    return "Full dates range, all types"


def _get_member(name):
    return Member.objects.get(first_name=name) if Member.objects.filter(first_name=name).exists() else None


def _get_proper_time(dateStr, timeStr, dateTimeStrFormat="%d %b %Y %H:%M"):
    dateToReturn = datetime.strptime(dateStr + " " + timeStr, dateTimeStrFormat)
    return timezone("Europe/Stockholm").localize(dateToReturn)


def _get_status(param):
    return "RESOLVED" if ("fix" in param.lower() or "answer" in param.lower() or "consul" in param.lower()) else "POSTPONED"


def _get_type(param):
    return "REMOTE" if "remote" in param.lower() else "ONSITE"
