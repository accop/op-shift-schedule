from django.urls import path
from oncall import views

app_name = "oncall"

urlpatterns = [
    path("", views.oncall_log_view, name="oncall_log"),
    path("<int:id>/", views.oncall_detail, name="oncall_detail"),
    path("call/create/", views.call_create_view, name="oncall_log_create"),
    path("call/colors/", views.get_domain_colors, name="oncall_colors"),
    path("call/upload/", views.call_upload_list_post, name="oncall_upload_list_post"),
    path("call/stat/by_function/", views.call_stat_by_function, name="oncall_stat_by_function"),
    path("call/stat/duration/", views.call_duration_histogram, name="oncall_duration_histogram"),
    path("call/stat/time-of-day/", views.call_time_of_day_histogram, name="oncall_time_of_day_histogram"),
    path("call/stat/daily-cumulative/", views.call_daily_cumulative, name="oncall_daily_cumulative"),
    path("ajax/oncall_log", views.oncall_log_contacts_json, name="oncall_log_json"),
    path("<int:id>/edit/", views.oncall_update_view, name="oncall_log_edit"),
]
