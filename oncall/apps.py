from django.apps import AppConfig


class OncallConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "oncall"
