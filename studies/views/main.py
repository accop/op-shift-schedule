from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from studies.models import *
from studies.forms import StudyRequestForm, StudyRequestFormClosing
import django.contrib.messages as messages
from django.views import View
from django.utils import timezone
import csv
from members.models import Member
from datetime import datetime

from shifter.notifications import notificationService


def page_not_found(request, exception):
    return render(request, "404.html", {})


class StudyView(View):
    def get(self, request):
        form = StudyRequestForm({"member": request.user}, user=request.user)
        closing_form = StudyRequestFormClosing()
        context = {
            "form": form,
            "closing_form": closing_form,
            "defaultDate": timezone.now().date().strftime(DATE_FORMAT),
            "hide_extra_role_selection": True,
        }
        return render(request, "request.html", context)

    def post(self, request):
        form = StudyRequestForm(request.POST, user=request.user)
        if form.is_valid():
            col = form.cleaned_data["collaborators"]
            post = form.save(commit=False)
            post.booking_created = timezone.localtime(timezone.now())
            post.booked_by = request.user
            post.save()
            post.collaborators.set(col)
            message = "Study Booking {} for {} on {}, is added!".format(post.title, post.member.first_name, post.booking_created)
            messages.success(request, message)
            notificationService.notify(post)
        else:
            message = "Study form is not valid, please correct."
            messages.success(request, message)
        return redirect("studies:study_request")


class SingleStudyView(View):
    def get(self, request, sid=None):
        form = StudyRequestForm({"member": request.user}, user=request.user)
        closing_form = StudyRequestFormClosing()
        context = {"form": form, "closing_form": closing_form}
        if sid is not None:
            try:
                study = StudyRequest.objects.select_related("campaign").get(id=sid)
                context["study"] = StudyRequest.objects.get(id=sid)
                context["campaign"] = study.campaign
            except StudyRequest.DoesNotExist:
                messages.error(request, "No such study found with the given id={}".format(sid))
                pass
        return render(request, "study.html", context)


@require_http_methods(["POST"])
def studies_close(request):
    booking_id = request.POST.get("booking_id")
    form = StudyRequestFormClosing(request.POST)
    if form.is_valid():
        comment = form.cleaned_data["after_comment"]
        link = form.cleaned_data["logbook_link"]
        status = form.cleaned_data["status"]
        current_booking = get_object_or_404(StudyRequest, id=booking_id)
        current_booking.booking_finished = timezone.localtime(timezone.now())
        current_booking.after_comment = comment
        current_booking.logbook_link = link
        current_booking.state = status
        current_booking.finished_by = request.user
        current_booking.save()
        message = "Booking for {} on {}, was updated!".format(current_booking.member.first_name, current_booking.title)
        messages.success(request, message)
    else:
        message = "Booking form is not valid, please correct."
        messages.success(request, message)
    return redirect("studies:study_request")


def import_requests(request):
    def get_state_key(state_description):
        state_mapping = {"Planned": "P", "Requested": "R", "Booked": "B", "Canceled": "C", "Done": "D"}
        return state_mapping.get(state_description, None)

    def parse_date(date_str):
        return datetime.strptime(date_str, DATE_FORMAT_FULL)

    if request.method == "POST":
        csv_file = request.FILES.get("csv_file")
        if csv_file:
            data = csv_file.read().decode("utf-8").splitlines()
            reader = csv.DictReader(data)

            for row in reader:
                state_key = get_state_key(row["State"])
                if not state_key:
                    messages.error(request, f"Invalid state value: {row['State']} in CSV. Row skipped.")
                    continue

                booked_by_user = Member.objects.get(first_name=row["# name + bookedby"])
                if not booked_by_user:
                    messages.error(request, f"User {row['# name + bookedby']} not found. Skipping.")
                    continue

                slot_start = parse_date(row["slot start"])
                slot_end = parse_date(row["slot end"])

                if not slot_start or not slot_end:
                    messages.error(request, f"Invalid date format for start or end time in row: {row}")
                    continue

                collaborators_names = row["Many users"].split(":")
                collaborators = []

                for name in collaborators_names:
                    collaborator = Member.objects.get(first_name=name.strip())
                    if collaborator:
                        collaborators.append(collaborator)

                study_request = StudyRequest(
                    title=row["Title"],
                    description=row["Description"],
                    state=state_key,
                    booked_by=booked_by_user,
                    member=booked_by_user,
                    slot_start=slot_start,
                    slot_end=slot_end,
                    booking_created=timezone.now(),
                )
                study_request.save()
                if collaborators:
                    study_request.collaborators.set(collaborators)

            messages.success(request, "CSV file processed successfully.")
            return redirect("studies:study_request")

    return render(request, "your_template.html")
