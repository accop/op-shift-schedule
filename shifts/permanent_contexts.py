from .activeshift import prepare_active_crew
from .models import Contact, ShiftForMarket

import django.contrib.messages as messages
from members.models import Team

import os

from guardian.shortcuts import get_objects_for_user
from shifter.settings import (
    MAIN_PAGE_HOME_BUTTON,
    APP_REPO,
    CONTROL_ROOM_PHONE_NUMBER,
    WWW_EXTRA_INFO,
    SHIFTER_PRODUCTION_INSTANCE,
    SHIFTER_TEST_INSTANCE,
    STOP_DEV_MESSAGES,
)


def operation_crew_context(request):
    active_shift = prepare_active_crew(dayToGo=None, slotToGo=None, hourToGo=None, fullUpdate=False)
    the_current_crew = active_shift["currentTeam"]
    for people in the_current_crew:
        people.real_role = people.role.name if people.role is not None else people.member.role.name
    return {"operation_crew": the_current_crew}


def useful_contact_context(request):
    contacts = Contact.objects.filter(active=True)
    return {"useful_contact": contacts}


def shift_market_count(request):
    if request.user.is_authenticated:
        shift_count = ShiftForMarket.objects.filter(is_available=True, shift__member__role=request.user.role).count()
        return {"shift_count": shift_count}
    return {}


def nav_bar_context(request):
    teams = Team.objects.all().order_by("name")
    return {"teams": teams}


def rota_maker_role(request):
    rota_maker_for = get_objects_for_user(request.user, "members.view_desiderata")
    return {"rota_maker_for": rota_maker_for}


def application_context(request):
    if SHIFTER_TEST_INSTANCE and not STOP_DEV_MESSAGES:
        messages.warning(
            request,
            """<strong>This is a DEVELOPMENT instance</strong>
                         In order to find current schedules, please refer to <a href="{}">the production instance</a>
                         """.format(
                SHIFTER_PRODUCTION_INSTANCE
            ),
        )

    stream = os.popen("git describe --tags")
    git_tag = stream.read()
    context = {
        "APP_NAME": MAIN_PAGE_HOME_BUTTON,
        "APP_REPO": APP_REPO,
        "SHIFTER_TEST_INSTANCE": SHIFTER_TEST_INSTANCE,
        "APP_GIT_TAG": git_tag,
        "controlRoomPhoneNumber": CONTROL_ROOM_PHONE_NUMBER,
        "wwwWithMoreInfo": WWW_EXTRA_INFO,
    }
    return context
