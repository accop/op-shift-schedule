import csv
import io
import django.contrib.messages as messages
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.db import IntegrityError
from shifts.models import *


class ImportResult:
    uploadSuccessful = False
    affectedMembers = []
    totalLinesAdded = 0
    date = None
    shiftFullDate = None
    tagName = None


def importShiftsFromCSV(request, defaultShiftRole, campaign, revision) -> ImportResult:
    totalLinesAdded = 0
    uploadSuccessful = True
    affectedMembers = []
    shiftRole = defaultShiftRole

    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith(".csv"):
            messages.error(request, "Wrong file type! Needs to be CSV")
            return HttpResponseRedirect(reverse("shifter:shift-upload"))
        if csv_file.multiple_chunks():
            messages.error(request, "Too large file")
            return HttpResponseRedirect(reverse("shifter:shift-upload"))

        file_data = csv_file.read().decode("utf-8")
        date_txt = csv_file.name.replace(".csv", "").split("__")[1]
        date = datetime.datetime.fromisoformat(date_txt)
        lines = file_data.split("\n")
        for lineIndex, line in enumerate(lines):
            fields = line.split(",")
            nameFromFile = fields[0].replace(" ", "")
            for dayIndex, one in enumerate(fields):
                if dayIndex == 0:
                    continue
                one.replace(" ", "")
                slotAbbrev = one.strip()
                if slotAbbrev == "" or slotAbbrev == "-":  # neutral shift slot abbrev
                    continue
                shiftDetails = one.split(":")
                if len(shiftDetails):  # index 0-> name, index -> shift role
                    slotAbbrev = shiftDetails[0]
                    if len(shiftDetails) > 1:
                        if shiftDetails[1] == "-" or shiftDetails[1] == "VAC":
                            shiftRole = None
                        else:
                            shiftRole = ShiftRole.objects.filter(abbreviation=shiftDetails[1]).first()
                            if shiftRole is None:
                                messages.error(
                                    request,
                                    "Cannot find role defined like {} for at line \
                                                            {} and column {} (for {}), using default one {}.".format(
                                        shiftDetails[1], lineIndex, dayIndex, nameFromFile, defaultShiftRole
                                    ),
                                )
                                shiftRole = defaultShiftRole
                    if shiftDetails[-1] == "VAC":
                        is_vacation = True
                        is_active = False
                    else:
                        is_vacation = False
                        is_active = True

                shiftFullDate = date + datetime.timedelta(days=dayIndex - 1)
                shift = Shift()
                try:
                    member = Member.objects.get(first_name=nameFromFile)
                    slot = Slot.objects.get(abbreviation=slotAbbrev.strip())
                    shift.campaign = campaign
                    shift.role = shiftRole
                    shift.revision = revision
                    shift.date = shiftFullDate
                    shift.slot = slot
                    shift.member = member
                    shift.is_vacation = is_vacation
                    shift.is_active = is_active
                    shift.csv_upload_tag = csv_file.name
                    totalLinesAdded += 1
                    shift.save()
                    affectedMembers.append(member)
                    shiftRole = defaultShiftRole
                except ObjectDoesNotExist:
                    # print(e)
                    # print("'{}' '{}' ".format(nameFromFile, slotAbbrev))
                    if "#" in fields[0] or "#" in one:  # skip hashed cell/line
                        continue
                    messages.error(
                        request,
                        "Could not find system member for ({}) / slot ({}), in line {} column {}.\
                                    Skipping for now Check your file".format(
                            fields[0], one, lineIndex, dayIndex
                        ),
                    )
                    uploadSuccessful = False

                except IntegrityError as e:
                    print(e)
                    uploadSuccessful = False
                    messages.error(
                        request,
                        "Could not add member {} for {} {}, \
                                            Already in the system for the same \
                                            role: {}  campaign: {} and revision {}".format(
                            fields[0], shiftFullDate, one, shiftRole, campaign, revision
                        ),
                    )

    except Exception as e:
        messages.error(request, "Unable to upload file. Critical error, see {}".format(e))
        uploadSuccessful = False

    importResult = ImportResult()
    importResult.uploadSuccessful = uploadSuccessful
    importResult.tagName = csv_file.name
    importResult.totalLinesAdded = totalLinesAdded
    importResult.affectedMembers = affectedMembers
    return importResult


def findStartEndDate(shifts):
    return None, None


def findDistinctMembers(shifts):
    return []


def everyDayFrom(startDate, endDate):
    return []


def exportShiftsToCSV(shifts):
    startDate, endDate = findStartEndDate(shifts)
    # members = findDistinctMembers(shifts)
    for oneDate in everyDayFrom(startDate, endDate):
        pass


def exportDesideratasToCSV(desideratas):
    pass


def write_csv(data, headers, filename):
    output = io.StringIO()
    writer = csv.writer(output)

    writer.writerow(headers)

    for row in data:
        writer.writerow(row)

    response = HttpResponse(output.getvalue(), content_type="text/csv")
    response["Content-Disposition"] = f"attachment; filename={filename}"
    return response
