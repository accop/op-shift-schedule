from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404, Http404, redirect
from django.core.exceptions import PermissionDenied

from django.template.loader import render_to_string
import django.contrib.messages as messages
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_http_methods, require_safe
from django.db.models import Q
from django.views import View

import members.models
from members.models import Team
from shifts.hrcodes import find_current_demarcation_range
from shifts.models import *
from assets.models import *

from studies.models import *
from assets.forms import AssetBookingForm, AssetBookingFormClosing
import datetime
import phonenumbers
from shifts.activeshift import prepare_active_crew, prepare_for_JSON
from shifts.contexts import prepare_default_context, prepare_user_context, get_default_backup_revision
from shifter.settings import DEFAULT_SHIFT_SLOT
from shifts.workinghours import find_working_hours, find_daily_rest_time_violation, find_weekly_rest_time_violation
from shifts.exchanges import (
    is_valid_for_hours_constraints,
    perform_exchange_and_save_backup,
    perform_simplified_exchange_and_save_backup,
    is_valid_for_hours_constraints_from_market,
)
from shifts.io import importShiftsFromCSV

from django.utils import timezone
from shifter.notifications import notificationService, NewShiftsUpload


@require_safe
def index(request, team_id=-1):
    revisions = Revision.objects.filter(valid=True).order_by("-number")
    team = None
    if team_id > 0:
        team = get_object_or_404(Team, id=team_id)
    return prepare_main_page(request, revisions, team)


@login_required
def my_team(request):
    revisions = Revision.objects.filter(valid=True).order_by("-number")
    team = request.user.team
    return prepare_main_page(request, revisions, team, my_team=True)


def prepare_main_page(request, revisions, team, revision=None, filtered_campaigns=None, all_roles=False, my_team=False):
    if revision is None:
        revision = revisions.first()
    someDate = datetime.datetime.now() + datetime.timedelta(days=-61)  # default - always last two months
    scheduled_campaigns = Campaign.objects.filter(revision=revision).filter(date_end__gt=someDate)
    if filtered_campaigns is not None:
        scheduled_campaigns = Campaign.objects.filter(revision=revision).filter(id__in=filtered_campaigns)
    months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    month_as_int = datetime.datetime.now().month
    previous_month_as_int = months[months.index(month_as_int) - 1] if month_as_int > 1 else 12
    next_month_as_int = months[months.index(month_as_int) + 1] if month_as_int < 12 else 1
    context = {
        "revisions": revisions,
        "displayed_revision": revision,
        "campaigns": Campaign.objects.filter(revision=revision),
        "scheduled_campaigns_list": scheduled_campaigns,
        "all_roles": all_roles,
        "team": team,
        "my_team": my_team,
        "shift_slots": [] if not my_team else Slot.objects.filter(used_for_lookup=True).order_by("hour_start"),
        "current_month": [-1, ""] if not my_team else [month_as_int, datetime.date(1900, month_as_int, 1).strftime("%B")],
        "previous_month": [-1, ""] if not my_team else [previous_month_as_int, datetime.date(1900, previous_month_as_int, 1).strftime("%B")],
        "next_month": [-1, ""] if not my_team else [next_month_as_int, datetime.date(1900, next_month_as_int, 1).strftime("%B")],
    }
    return render(request, "team_view.html", prepare_default_context(request, context))


@require_safe
def dates(request):
    context = {
        "campaigns": Campaign.objects.all(),
        "slots": Slot.objects.all().order_by("hour_start"),
        "shiftroles": ShiftRole.objects.all(),
        "memberroles": members.models.Role.objects.all(),
        "assets": Asset.objects.all(),
    }
    return render(request, "dates.html", prepare_default_context(request, context))


@login_required
@require_http_methods(["POST"])
@csrf_protect
def dates_slots_update(request):
    if not request.user.is_superuser:
        raise PermissionDenied
    count = 0
    for slot in Slot.objects.all():
        slot.op = False
        toUpdate = request.POST.get(slot.abbreviation, None)
        if toUpdate is not None:
            slot.op = True
            count = +1
            messages.success(request, "Slot {} is operational now!".format(slot))
        slot.save()
    if count == 0:
        s = Slot.objects.filter(abbreviation=DEFAULT_SHIFT_SLOT).first()
        s.op = True
        s.save()
        messages.success(request, "There must be at leasst one slot set to operational, setting default {} ".format(s))
    return HttpResponseRedirect(reverse("shifter:dates"))


@require_safe
def todays(request):
    dayToGo = request.GET.get("date")
    slotToGo = request.GET.get("slot")
    hourToGo = request.GET.get("hour")
    fullUpdate = "fullUpdate" in request.GET

    activeShift = prepare_active_crew(dayToGo=dayToGo, slotToGo=slotToGo, hourToGo=hourToGo, fullUpdate=fullUpdate)

    now = datetime.datetime.now()

    earliestSlotHourStart = Slot.objects.filter(op=True).order_by("hour_start").first().hour_start
    if now.time() <= earliestSlotHourStart:
        today_date = now.date() - datetime.timedelta(days=1)
    else:
        today_date = now.date()

    today_start = datetime.datetime.combine(today_date, datetime.time(hour=0, minute=0, second=1))
    today_end = datetime.datetime.combine(today_date, datetime.time(hour=23, minute=59, second=0))

    scheduled_studies = StudyRequest.objects.filter(
        state__in=["B", "D"],
        slot_start__gte=today_start,
        slot_end__lte=today_end,
    ).order_by("slot_start", "priority")

    context = {
        "today": activeShift.get("today"),
        "checkTime": activeShift.get("today", now).time(),
        "activeSlot": activeShift.get("activeSlot"),
        "activeSlots": activeShift.get("activeSlots", []),
        "currentTeam": activeShift.get("currentTeam", []),
        "shiftID": activeShift.get("shiftID"),
        "activeStudies": scheduled_studies,
    }

    nowShift = next(iter(context["currentTeam"]), None)

    if nowShift:
        next_team_query = (
            Shift.objects.filter(revision=nowShift.revision, is_active=True)
            .filter(
                Q(date=nowShift.date, slot__hour_start__gt=nowShift.slot.hour_start, role=None)
                | Q(date=nowShift.date + datetime.timedelta(days=1), slot__hour_start__gte=earliestSlotHourStart, role=None)
            )
            .order_by("date", "slot__hour_start")
        )

        context["nextTeam"] = next_team_query

    return render(request, "today.html", prepare_default_context(request, context))


@require_safe
@login_required
def user(request, u=None, rid=None):
    if u is None:
        member = request.user
    else:
        member = Member.objects.filter(id=u).first()
    # ss = Shift.objects.filter(revision=Revision.objects.filter(valid=True).order_by("-number").first()).filter(member=member)

    today = datetime.datetime.now()
    year = today.year
    month = today.month
    default_start = datetime.date(year, month, 1)
    if month > 11:
        default_end = datetime.date(year + 1, 1, 1) + datetime.timedelta(days=-1)
    else:
        default_end = datetime.date(year, month + 1, 1) + datetime.timedelta(days=-1)
    context = prepare_user_context(member, revisionNext=rid)
    context["revisions"] = Revision.objects.order_by("-number")
    context["member"] = member
    context["default_start"] = default_start
    context["default_end"] = default_end
    context["hide_campaign_selection"] = True
    context["hide_extra_role_selection"] = True
    context["show_companion"] = True
    context["the_url"] = reverse("ajax.get_user_events")
    context["the_market_url"] = reverse("ajax.get_market_events")
    context["unread_notifications"] = member.notifications.unread()
    if rid is not None:
        requested_revision = get_object_or_404(Revision, number=rid)
        revision = Revision.objects.filter(valid=True).order_by("-number").first()
        if requested_revision not in Revision.objects.filter(date_start__gt=revision.date_start).filter(ready_for_preview=True).filter(merged=False).order_by(
            "-number"
        ):
            raise Http404
        messages.warning(request, "On top of the current schedule, you're seeing revision '{}'".format(requested_revision))
        context["requested_future_rev_id"] = rid

    shiftExchanges = ShiftExchange.objects.filter(
        Q(requestor=member) | Q(shifts__shift_for_exchange__member__exact=member) | Q(shifts__shift__member__exact=member)
    ).order_by("-requested")

    shiftExchangesLast = ShiftExchange.objects.filter(requestor=member, tentative=True, applicable=True).order_by("-requested").first()
    # FIXME this one is to clear the 'over select' from the query above that
    shiftExchangesUnique = []
    for se in shiftExchanges:
        if se not in shiftExchangesUnique:
            shiftExchangesUnique.append(se)
    pendingRequests = 0
    for se in shiftExchangesUnique:
        bb = is_valid_for_hours_constraints(shiftExchange=se, member=member, baseRevision=Revision.objects.filter(valid=True).order_by("-number").first())
        se.applicable = bb[0]
        if not bb[0]:
            se.tentative = False
        se.save()
        if se.applicable and not se.implemented:
            pendingRequests += 1
    context["shift_exchanges_requested"] = shiftExchangesUnique
    context["shiftExchangesLastId"] = None if shiftExchangesLast is None else shiftExchangesLast.id
    context["exchanges_total"] = pendingRequests
    context["today"] = today.strftime(SIMPLE_DATE)
    context["demarcation_start"] = find_current_demarcation_range()[0].strftime(SIMPLE_DATE)
    # when there is OPEN shift swap request
    # TODO CRITICAL for some reason this Id is rendered (if not NONE) in the user.html as "(id,)" not as "id"
    # print(context["shiftExchangesLastId"])
    return render(request, "user.html", prepare_default_context(request, context))


@require_http_methods(["POST"])
@csrf_protect
@login_required
def user_notifications(request, uid=None):
    m = Member.objects.get(id=uid)
    try:
        # a = request.POST["notificationShifts"]
        m.notification_shifts = True
    except:
        m.notification_shifts = False
    try:
        # a = request.POST["notificationStudies"]
        m.notification_studies = True
    except:
        m.notification_studies = False
    m.save()
    messages.success(request, "Notification settings are updated!")
    return HttpResponseRedirect(reverse("shifter:user"))


@login_required
def shiftExchangePerform(request, ex_id=None):
    shiftExchanges = ShiftExchange.objects.filter(id=ex_id, implemented=False)
    if shiftExchanges.count() == 0:
        messages.error(request, "This shift exchange either does not exits or was already finalised!")
        return user(request)
    shift_exchange = shiftExchanges.first()
    if shift_exchange.approver == request.user and not shift_exchange.tentative:
        perform_exchange_and_save_backup(shift_exchange, approver=request.user, revisionBackup=shift_exchange.backupRevision)
        messages.success(request, "Requested shift exchange is now successfully implemented. If you need to revert it please " "contact rota maker.")
        notificationService.notify(shift_exchange)
    else:
        messages.error(request, "This Exchange can only be approved by {}".format(shift_exchange.approver))
    return HttpResponseRedirect(reverse("shifter:user"))


@login_required
def shiftExchangeRequestCancel(request, ex_id=None):
    shiftExchanges = ShiftExchange.objects.filter(id=ex_id, implemented=False)
    if shiftExchanges.count() == 0:
        messages.error(request, "This shift exchange either does not exits or was already finalised!")
        return user(request)
    shift_exchange = shiftExchanges.first()
    if shift_exchange.requestor == request.user and not shift_exchange.implemented:
        shift_exchange.delete()
        messages.success(request, "Shift exchange request {} deleted".format(ex_id))
    else:
        messages.error(request, "You are not authorised to cancel that request contact {} instead".format(shift_exchange.requestor))
    return HttpResponseRedirect(reverse("shifter:user"))


@login_required
def shiftExchangeView(request, ex_id=None):
    sEx = ShiftExchange.objects.get(id=ex_id)
    r = Revision.objects.filter(valid=True).order_by("-number")[0]

    s = ShiftPlaceHolder()
    s2 = ShiftPlaceHolder()

    for one in sEx.shifts.all():
        try:
            s = Shift.objects.get(revision=r, member=one.shift.member, slot=one.shift_for_exchange.slot, date=one.shift_for_exchange.date)
        except ObjectDoesNotExist:
            pass
        try:
            s2 = Shift.objects.get(revision=r, member=one.shift_for_exchange.member, slot=one.shift.slot, date=one.shift.date)
        except ObjectDoesNotExist:
            pass
    #  TODO Fix the display issue for MORE than one shift in the swap
    context = {
        "sEx": sEx,
        "oldShiftRequestor": one.shift,
        "oldShiftRequested": one.shift_for_exchange,
        "newShiftRequestor": s2,
        "newShiftRequested": None if isinstance(s, ShiftPlaceHolder) else s,
    }
    return render(request, "shiftexchange_edit.html", prepare_default_context(request, context))


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shiftExchangeRequestCreateOrUpdate(request, ex_id=None):
    s1ID = int(request.POST.get("futureMy", -1))
    s2ID = int(request.POST.get("futureOther", -1))
    if s1ID < 1 or s2ID < 1:
        messages.error(request, "There was an issue with the selected shifts")
        return HttpResponseRedirect(reverse("shifter:user"))

    otherShift = Shift.objects.get(id=s2ID)
    sPair = ShiftExchangePair.objects.create(shift=Shift.objects.get(id=s1ID), shift_for_exchange=otherShift)

    # FIXME Fix the bug with the non refreshing redirect and no ex_id given in request
    if ex_id is None:
        sEx = ShiftExchange()
        sEx.requestor = request.user
        sEx.approver = otherShift.member
        sEx.backupRevision = get_default_backup_revision()
        # sEx.backupRevision = Revision.objects.filter(number=1).first()
        sEx.requested = timezone.now()
        sEx.save()
        sPair.save()
        sEx.shifts.add(sPair)
        sEx.save()
    else:
        sEx = ShiftExchange.objects.get(id=ex_id)
        if sEx.approver != otherShift.member:
            messages.error(
                request, "Cannot add the Exchange request {} with {}. Current request can be updated only for {}".format(sEx, otherShift.member, sEx.approver)
            )
            return HttpResponseRedirect(reverse("shifter:user"))
        sPair.save()
        sEx.shifts.add(sPair)
        sEx.save()

    messages.success(request, "Created/Updated the Exchange request {} with {}".format(sEx, sPair))
    return HttpResponseRedirect(reverse("shifter:user"))


@require_http_methods(["GET"])
@csrf_protect
@login_required
def shiftExchangeRequestClose(request, ex_id=None):
    sEx = ShiftExchange.objects.get(id=ex_id)
    if sEx.requestor == request.user and sEx.tentative:
        notificationService.notify(sEx)
        sEx.tentative = False
        sEx.save()
        messages.success(request, "Closed the Exchange request {}, now awaiting for {}".format(sEx, sEx.approver))
    elif not sEx.tentative or sEx.implemented:
        messages.error(request, "This Exchange is already handled")
    else:
        messages.error(request, "This Exchange can only be handled by {}".format(sEx.approver))
    return HttpResponseRedirect(reverse("shifter:user"))


@login_required()
def users(request):
    # Requesting all ACTIVES user, but anonymous
    first_name = Q(first_name__exact="")
    last_name = Q(last_name__exact="")
    users_list = Member.objects.filter(is_active=True).exclude(first_name & last_name)
    users_requested = request.GET.get("u", "")
    users_requested = users_requested.split(",")
    users_requested = [int(x) for x in users_requested] if users_requested != [""] else []
    revisions = Revision.objects.filter(valid=True).order_by("-number")
    revisions_future = Revision.objects.filter(ready_for_preview=True).filter(merged=False).order_by("-number")
    revision = revisions.first()
    someDate = datetime.datetime.now() + datetime.timedelta(days=-61)  # default - always last two months
    scheduled_campaigns = Campaign.objects.filter(revision=revision).filter(date_end__gt=someDate)
    roles = ShiftRole.objects.all()

    context = {
        "revisions_future": revisions_future,
        "revisions": revisions,
        "displayed_revision": None,
        "campaigns": Campaign.objects.filter(revision=revision),
        "scheduled_campaigns_list": scheduled_campaigns,
        "users": users_list,
        "users_requested": users_requested,
        "hide_studies": True,
        "allow_see_canceled": True,
        "roles": roles,
    }
    return render(request, "users.html", prepare_default_context(request, context))


@require_safe
def icalendar(request):
    member = None
    team = None
    if request.GET.get("mid"):
        member = Member.objects.filter(id=request.GET.get("mid")).first()
    if request.GET.get("tid"):
        team = Team.objects.filter(id=request.GET.get("tid")).first()

    revision = Revision.objects.filter(valid=True).order_by("-number").first()

    shifts = Shift.objects.filter(member=member, revision=revision, is_active=True).order_by("-date")[:365]
    from .ajax import _get_companions_shift

    companionShifts = _get_companions_shift(member, shifts)
    companionShiftsDates = {one.start: [one, []] for one in shifts}
    for one in companionShifts:
        companionShiftsDates[one.start][1].append(one)

    studies = StudyRequest.objects.filter(member=member, state__in=["B", "D"])
    studies_as_collaborator = StudyRequest.objects.filter(collaborators=member, state__in=["B", "D"])
    if team is not None:
        shifts = (
            Shift.objects.filter(
                member__team=team,
                revision=revision,
                is_active=True,
                date__gt=timezone.now() - datetime.timedelta(days=60),
                date__lt=timezone.now() + datetime.timedelta(days=31 * 8),
            )
            .exclude(slot__abbreviation=DEFAULT_SHIFT_SLOT)
            .order_by("-date")
        )

        companionShifts = _get_companions_shift(None, shifts)
        companionShiftsDates = {one.start: [one, []] for one in shifts}
        for one in companionShifts:
            companionShiftsDates[one.start][1].append(one)

        studies = []
        studies_as_collaborator = []

    context = {
        "campaign": "Exported Shifts",
        "companionShifts": list(companionShiftsDates.values()),
        "studies": studies,
        "studies_as_collaborator": studies_as_collaborator,
        "member": member,
        "team": team if not None else member.team,
        "now": timezone.now(),
    }

    body = render_to_string("icalendar.ics", context)
    body = body.rstrip()
    return HttpResponse(body.replace("\n", "\r\n"), content_type="text/calendar")


# from django import template

# register = template.Library()
# @register.filter
# def format_ical(text):
#     """Formats text for iCalendar DESCRIPTION field with escaped newlines."""
#     if text:
#         return text.replace('\r\n', '\\n').replace('\n', '\\n').replace('\\n', '\r\n ')
#     return ""


@require_safe
@login_required
def icalendar_view(request):
    month = None
    monthLabel = request.GET.get("month")
    if request.GET.get("month"):
        if monthLabel == "current":
            month = datetime.datetime.now()
        if monthLabel == "next":
            month = datetime.datetime.now() + datetime.timedelta(30)
    member = None
    if request.GET.get("member"):
        member = Member.objects.filter(id=request.GET.get("member")).first()

    if month is None or member is None:
        # TODO render error page
        pass

    monthFirstDay = month.replace(day=1).date()
    next_month = monthFirstDay.replace(day=28) + datetime.timedelta(days=4)
    monthLastDay = next_month.replace(day=1) + datetime.timedelta(days=-1)
    revision = Revision.objects.filter(valid=True).order_by("-number").first()

    shifts = Shift.objects.filter(date__lte=monthLastDay, date__gte=monthFirstDay).filter(member=member, revision=revision)

    StudyFirstDay = datetime.datetime.combine(monthFirstDay, datetime.time(hour=0, minute=0, second=1, microsecond=0))
    StudyLasttDay = datetime.datetime.combine(monthLastDay, datetime.time(hour=23, minute=59, second=59, microsecond=0))
    studies = StudyRequest.objects.filter(slot_end__lte=StudyLasttDay, slot_start__gte=StudyFirstDay).filter(member=member, state__in=["B", "D"])

    context = {
        "campaign": "Exported Shifts",
        "shifts": shifts,
        "studies": studies,
        "member": member,
    }
    body = render_to_string("icalendar.ics", context)
    # Note iCalendar format requires CR/LF line endings
    return HttpResponse(body.replace("\n", "\r\n"), content_type="text/calendar")


@require_safe
def ioc_update(request):
    """
    Expose JSON with current shift setup. To be used by any script/tool to update the IOC
    """
    dayToGo = request.GET.get("date", None)
    slotToGo = request.GET.get("slot", None)
    hourToGo = request.GET.get("hour", None)
    fullUpdate = request.GET.get("fullUpdate", None) is not None
    activeShift = prepare_active_crew(dayToGo=dayToGo, slotToGo=slotToGo, hourToGo=hourToGo, fullUpdate=fullUpdate)
    dayDate = activeShift.get("today", datetime.datetime.today()).date()
    dayStudiesStart = datetime.datetime.combine(dayDate, datetime.time(hour=0, minute=0, second=1, microsecond=0))
    dayStudiesEnd = datetime.datetime.combine(dayDate, datetime.time(hour=23, minute=59, second=59, microsecond=0))
    studies = StudyRequest.objects.filter(state="B", slot_start__gte=dayStudiesStart, slot_start__lte=dayStudiesEnd).order_by("slot_start")

    return JsonResponse(prepare_for_JSON(activeShift, studies=studies))


@require_safe
def shifts(request):
    shiftId = request.GET.get("id", None)
    if shiftId is not None:
        dataToReturn = {"SID": shiftId, "status": False}
        shiftIDs = ShiftID.objects.filter(label=shiftId)
        if len(shiftIDs):
            shiftID = shiftIDs.first()
            previousShiftId = None
            try:
                previousShiftId = ShiftID.objects.get(id=shiftID.id - 1).label
            except ObjectDoesNotExist:
                pass
            nextShiftId = None
            try:
                nextShiftId = ShiftID.objects.get(id=shiftID.id + 1).label
            except ObjectDoesNotExist:
                pass
            dataToReturn = {"SID": shiftId, "status": "True", "prev": previousShiftId, "next": nextShiftId}
    else:
        shiftIds = ShiftID.objects.all().order_by("-label")
        dataToReturn = {"ids": [id.label for id in shiftIds]}
    return JsonResponse(dataToReturn)


@require_safe
def scheduled_work_time(request):
    rev = Revision.objects.filter(valid=True).order_by("-number")[0]
    startDate = None
    endDate = None
    try:
        if request.GET.get("start", None) is not None:
            startDate = datetime.datetime.strptime(request.GET.get("start"), SIMPLE_DATE)
        if request.GET.get("end", None) is not None:
            endDate = datetime.datetime.strptime(request.GET.get("end"), SIMPLE_DATE)
        if request.GET.get("rev", None) is not None:
            revId = int(request.GET.get("rev"))
            rev = Revision.objects.filter(valid=True).filter(number=revId)[0]
    except ValueError:
        pass
    scheduled_shifts = Shift.objects.filter(revision=rev).filter(member__role__abbreviation="SL").order_by("date", "slot__hour_start", "member__role__priority")
    if startDate is not None and endDate is not None:
        scheduled_shifts = scheduled_shifts.filter(date__gte=startDate).filter(date__lte=endDate)
    dataToReturn = find_working_hours(scheduled_shifts, startDate=startDate, endDate=endDate)
    return JsonResponse(dataToReturn)


@require_safe
@login_required
def shifts_update(request):
    # add campaigns and revisions
    data = {"campaigns": Campaign.objects.all(), "revisions": Revision.objects.all(), "today": datetime.datetime.now().strftime(SIMPLE_DATE)}
    return render(request, "shifts_update.html", prepare_default_context(request, data))


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shifts_update_post(request):
    MERGE_TEST = False
    DELETE_TEST = False
    # merge one into another
    rev1Id = request.POST.get("revision-to-move", None)
    rev2Id = request.POST.get("revision-to-merge-in", None)
    mergeFrom = request.POST.get("date-to-merge-from", None)
    keepMergeFrom = True if "on" in request.POST.get("keep-shifts-to-merge-in", "off") else False
    # delete from one revision
    trimFrom = request.POST.get("date-to-delete-from", None)
    trimTo = request.POST.get("date-to-delete-until", None)
    revToTrimId = request.POST.get("revision-to-cut", None)
    # merge one into another
    if rev2Id is not None and rev1Id is not None and mergeFrom is not None:
        d = datetime.datetime.strptime(mergeFrom, SIMPLE_DATE).date()
        rev1 = Revision.objects.get(number=rev1Id)
        rev2 = Revision.objects.get(number=rev2Id)
        first_shift_in_rev1 = Shift.objects.filter(revision=rev1).order_by("date").first().start
        last_shift_in_rev1 = Shift.objects.filter(revision=rev1).order_by("-date").first().end
        s1 = Shift.objects.filter(revision=rev1, date__gte=first_shift_in_rev1)
        # membersInRevision1 = s1.values_list('member', flat = True).distinct()
        s2 = Shift.objects.filter(
            revision=rev2,
            date__gte=first_shift_in_rev1,
            date__lte=last_shift_in_rev1,
            #    member__in=membersInRevision1
        )
        try:
            backupRevision = Revision.objects.filter(name__startswith="BACKUP").first()
        except Exception:
            messages.error(request, "Make sure that one of the revisions is called BACKUP!")
            return HttpResponseRedirect(reverse("desiderata.team_view", kwargs={"team_id": request.user.team.id}))
        _find_and_remove_any_market(request, s1)
        _find_and_remove_any_market(request, s2)
        shifters = []
        campaign = None
        earlyDate = s1.order_by("date").first().start
        lateDate = s1.order_by("date").last().end
        if not keepMergeFrom:
            for shiftToBackup in s2:
                if not MERGE_TEST:
                    shiftToBackup.revision = backupRevision
                    shiftToBackup.save()
        for shiftToMerge in s1:
            if not MERGE_TEST:
                shiftToMerge.revision = rev2
                shiftToMerge.save()
            shifters.append(shiftToMerge.member)
            if campaign is None:
                campaign = shiftToMerge.campaign
        if not MERGE_TEST:
            rev1.merged = True
            rev1.save()
            messages.success(
                request,
                "Merged {} shifts from Revision {} in Revision: {} starting on date: {}. \n IF it was chosen {}: the removed "
                "shifts from {} are saved in {}.".format(s1.count(), rev1, rev2, d, not keepMergeFrom, rev2, backupRevision),
            )
            notificationService.notify(
                NewShiftsUpload(rev2, campaign, list(set(shifters)), rotaMaker=request.user, startEnd=(earlyDate, lateDate), revisionFrom=rev1)
            )
        if MERGE_TEST:
            print("Merge from :", rev1)
            print("Merge to   :", rev2)
            print("From       :", mergeFrom)
            print("Merge BOX ticked: ", keepMergeFrom)
            print(first_shift_in_rev1, " between ", last_shift_in_rev1)
            print("Processed shifts to merge: ", len(s1))
            print("Original Shifts in the time span: ", len(s2))
            print("Affected members: ", list(set(shifters)))

    # delete from one revision
    if revToTrimId is not None and trimFrom is not None:
        d = datetime.datetime.strptime(trimFrom, SIMPLE_DATE).date()
        d2 = None
        if trimTo is not None:
            d2 = datetime.datetime.strptime(trimTo, SIMPLE_DATE).date()
        revToTrim = Revision.objects.get(number=revToTrimId)
        s = Shift.objects.filter(revision=revToTrim, date__gte=d)
        if d2 is not None:
            s = s.filter(date__lte=d2)
        if not DELETE_TEST:
            _find_and_remove_any_market(request, s)
            for shiftToDelete in s:
                shiftToDelete.delete()
            messages.success(request, "Deleted {} shifts as from {} until {} in Revision: {}".format(s.count(), d, d2, revToTrim))
        if DELETE_TEST:
            print("Number of shifts to delete ", len(s))
    return HttpResponseRedirect(reverse("desiderata.team_view", kwargs={"team_id": request.user.team.id}))


def _find_and_remove_any_market(request, shifts: list):
    sfms = ShiftForMarket.objects.filter(shift__in=shifts)
    if len(sfms):
        messages.warning(request, "There are/were some shift for market related to those you want to delete, now they " "are deleted. ")
        for sfm in sfms:
            sfm.delete()


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shifts_update_status_post(request):
    print(request.POST)
    rev = Revision.objects.filter(valid=True).order_by("-number")[0]
    updateFrom = request.POST.get("date-to-update-from", None)
    updateTo = request.POST.get("date-to-update-until", None)
    shiftersIds = request.POST.getlist("displayed-shifters", None)
    status = int(request.POST.get("status-to-update", 0))
    new_slot_id = request.POST.get("new-slot-type", None)
    old_slot_id = request.POST.get("old-slot-type", None)
    new_role_id = request.POST.get("new-role-type", None)
    if updateFrom is not None and updateTo is not None and shiftersIds is not None:
        d1 = datetime.datetime.strptime(updateFrom, SIMPLE_DATE).date()
        d2 = datetime.datetime.strptime(updateTo, SIMPLE_DATE).date()
        if d2 < d1:
            messages.error(request, "Please correct the dates, cannot apply end date before the start one!")
            return HttpResponseRedirect(reverse("desiderata.team_view", kwargs={"team_id": request.user.team.id}))
        membersObj = Member.objects.filter(id__in=[int(i) for i in shiftersIds])
        filter = {
            "revision": rev,
            "date__gte": d1,
            "date__lte": d2,
            "member__in": membersObj,
        }
        slotFuture = None
        if status == 5:
            filter["slot__id"] = old_slot_id

            slotFuture = Slot.objects.get(id=new_slot_id)
            roleFuture = None if int(new_role_id) < 0 else ShiftRole.objects.get(id=new_role_id)
        shiftsToUpdate = Shift.objects.filter(**filter)
        for one in shiftsToUpdate:
            if status == 1:
                one.is_active = True
                one.is_cancelled = False
                one.is_vacation = False
            if status == 2:
                one.is_active = False
            if status == 3:
                one.is_cancelled = True
                one.is_active = False
            if status == 4:
                one.is_vacation = True
                one.is_active = False
                one.is_cancelled = False
            if status == 5:
                one.slot = slotFuture
                one.role = roleFuture
                one.is_active = True
                one.is_cancelled = False
                one.is_vacation = False
            one.save()
        messages.success(
            request, "Shifts ({}) for {} are updated between {} and {}".format(len(shiftsToUpdate), [a.name for a in membersObj], updateFrom, updateTo)
        )

    return HttpResponseRedirect(reverse("desiderata.team_view", kwargs={"team_id": request.user.team.id}))


@require_safe
@login_required
def shift_edit(request, sid=None):
    s = Shift.objects.get(id=sid)
    sfm = ShiftForMarket.objects.filter(shift=s).first()
    swap = None

    sExPairAsIs = ShiftExchangePair.objects.filter(Q(shift=s) | Q(shift_for_exchange=s))
    ss = Shift.objects.filter(revision=get_default_backup_revision(), slot=s.slot, date=s.date, is_active=True)
    sExPairExplicitBackup = ShiftExchangePair.objects.filter(Q(shift__in=ss) | Q(shift_for_exchange__in=ss))
    # print("old exPairs AS IS : ", sExPairAsIs)
    # print("old exPairs BACKUP: ", sExPairExplicitBackup)
    if len(sExPairAsIs) or len(sExPairExplicitBackup):
        try:
            swap = ShiftExchange.objects.get(shifts__in=sExPairAsIs)
            print("IN EXCHANGES AS IS")
            print(swap)
        except:
            print("IN EXCHANGES EXPLICIT BACKUP")
            swap = ShiftExchange.objects.filter(shifts__in=sExPairExplicitBackup).first()
            print(swap)
    # TODO in some complicated swaps the above may trigger 'returned 2'

    data = {
        "shift": s,
        "on_market": sfm,
        "swap": swap,
        "shiftRoles": ShiftRole.objects.all(),
        "replacement": Member.objects.filter(team=request.user.team, is_active=True).order_by("role"),
        "edit": True,
    }
    return render(request, "shift_edit.html", prepare_default_context(request, data))


@require_safe
@login_required
def shift_view(request, sid=None):
    s = Shift.objects.get(id=sid)
    sfm = ShiftForMarket.objects.filter(shift=s).first()
    data = {"shift": s, "on_market": sfm}
    return render(request, "shift_edit.html", prepare_default_context(request, data))


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shift_edit_post(request, sid=None):
    s = Shift.objects.get(id=sid)
    s.pre_comment = request.POST.get("preShiftComment", "")
    s.post_comment = request.POST.get("postShiftComment", "")

    s.is_cancelled = "cancelledLastMinute" in request.POST
    s.is_active = "activeShift" in request.POST
    s.is_vacation = "vacation" in request.POST

    try:
        s_role_id = int(request.POST.get("shiftRole", 0))
        if s_role_id > 0:
            s.role = ShiftRole.objects.get(id=s_role_id)
        else:
            s.role = None
    except (ValueError, ShiftRole.DoesNotExist):
        s.role = None

    s.save()
    url = reverse("shifter:shift-edit", kwargs={"sid": sid})
    messages.success(request, "<a class='href' href='{}'> Shift {} updated successfully! Edit again?</a>".format(url, s))
    return HttpResponseRedirect(reverse("shifter:index"))


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shift_edit_special_post(request, sid=None):
    print(request.POST)
    url = reverse("shifter:shift-edit", kwargs={"sid": sid})
    # FIXME maybe later add some clever check what needs to be set what not...
    # if request.POST.get("changed_on_date", None) is None and
    #   ("is_changed_urgent" in request.POST or "is_changed_urgent" in request.POST):
    #     messages.error(request, "Cannot save if date and is_changed is selected")
    #     return HttpResponseRedirect(url)
    s = Shift.objects.get(id=sid)
    s.changed_on_date = request.POST.get("changed_on_date", None)
    s.is_changed_urgent = "is_changed_urgent" in request.POST
    s.is_changed = "is_changed" in request.POST
    s.is_changed_offday = "is_changed_offday" in request.POST
    s.save()
    print(s)
    print(s.is_changed)
    print(s.is_changed_offday)
    print(s.is_changed_urgent)

    messages.success(request, "<a class='href' href='{}'> Shift {} updated successfully! Edit again?</a>".format(url, s))
    return HttpResponseRedirect(url)


@require_http_methods(["POST"])
@csrf_protect
@login_required
def market_shift_add(request, shift_id):
    shift = get_object_or_404(Shift, id=shift_id)
    member = request.user

    if shift.date < timezone.now().date():
        messages.error(request, "You cannot add a shift to the market with a date in the past.")
        return redirect("shifter:user")

    if ShiftForMarket.objects.filter(shift=shift, is_available=True).exists():
        messages.error(request, "This shift is already on the market.")
        return redirect("shifter:user")

    shiftRole = ShiftRole.objects.filter(name="Office")
    for role in shiftRole:
        print(role.name)

    if shift.slot.abbreviation == "NWH":
        messages.error(request, "This shift is not available for the market.")
        return redirect("shifter:user")

    market_comment = request.POST.get("marketComment", "")
    available_until = request.POST.get("availableUntil", None)
    urgency = request.POST.get("urgency", "Normal")

    if available_until:
        available_until_date = timezone.datetime.strptime(available_until, "%Y-%m-%d").date()

        if available_until_date < timezone.now().date():
            messages.error(request, "The 'available until' date cannot be in the past.")
            return redirect("shifter:user")
    else:
        available_until_date = None

    sfm = ShiftForMarket.objects.create(
        shift=shift,
        offerer=member,
        offered_date=timezone.now(),
        comments=market_comment,
        available_until=available_until_date,
        urgency=urgency,
        is_available=True,
        is_taken=False,
    )
    notificationService.notify(sfm)
    messages.success(request, "Shift successfully added to the market.")
    return redirect("shifter:shifts_market")


@login_required
def market_shifts(request):
    user_role = request.user.role
    show_all = request.GET.get("show_all")

    if show_all:
        available_shifts = ShiftForMarket.objects.filter(is_available=True).order_by("shift__date")
    else:
        available_shifts = ShiftForMarket.objects.filter(is_available=True, shift__member__role=user_role).order_by("shift__date")

    for a in available_shifts:
        a.check_if_available(timezone.now().date())

    if show_all:
        available_shifts = ShiftForMarket.objects.filter(is_available=True).order_by("shift__date")
    else:
        available_shifts = ShiftForMarket.objects.filter(is_available=True, shift__member__role=user_role).order_by("shift__date")

    shift_count = available_shifts.count()

    user_shifts = Shift.objects.filter(member=request.user, date__gt=timezone.now().date()).order_by("date")

    context = {
        "available_shifts": available_shifts,
        "user_shifts": user_shifts,
        "shift_count": shift_count,
        "show_all": show_all,
    }
    return render(request, "shifts_market.html", context)


@login_required
def market_shift_view(request, shift_id):
    context = {
        "available_shifts": [ShiftForMarket.objects.get(id=shift_id)],
    }
    return render(request, "shifts_market.html", context)


@login_required
def market_shift_take(request, shift_id):
    try:
        shift_market = ShiftForMarket.objects.get(id=shift_id, is_available=True)
    except ShiftForMarket.DoesNotExist:
        messages.error(request, "The shift you tried to take is no longer available.")
        return redirect("shifts_market")

    is_applicable = is_valid_for_hours_constraints_from_market(shift_market, shift_market.shift.revision, request.user)
    if not is_applicable[0]:
        messages.error(request, "Your schedule does not fit to accept this shift! Contact your rota maker to find out " "possible solutions!")
        return redirect("shifts_market")

    # FIXME think if that should go inside the 'simplified exchange'
    # for now, the 'my shift' on the same day are invalidated and put with comment
    shifts = Shift.objects.filter(date=shift_market.shift.date, member=request.user, revision=Revision.objects.filter(valid=True).order_by("-number").first())
    for shift in shifts:
        shift.is_active = False
        shift.pre_comment = "Removed with the market shift: {}".format(shift_market)
        shift.save()

    sExDone = perform_simplified_exchange_and_save_backup(
        shift_market.shift,
        request.user,
        requestor=shift_market.offerer,
        approver=request.user,
        revisionBackup=get_default_backup_revision(),
        exType="Market",
    )
    notificationService.notify(sExDone)
    shift_market.mark_as_taken()

    messages.success(request, "Requested shift exchange (update) is now successfully implemented.")
    messages.success(request, "You have successfully taken the shift.")
    return redirect("shifts_market")


@login_required
def market_shift_delete(request, shift_id):
    shift_market = get_object_or_404(ShiftForMarket, id=shift_id)

    if request.user != shift_market.offerer:
        messages.error(request, "You cannot delete this shift because you did not add it.")
        return redirect("shifts_market")

    shift_market.delete()
    messages.success(request, "Shift has been deleted from the market.")
    return redirect("shifts_market")


@login_required
def user_page(request):
    return redirect("user")


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shift_single_exchange_post(request, sid=None):
    s = Shift.objects.get(id=sid)
    m = Member.objects.get(id=int(request.POST["shiftMember"]))
    shift_slot = s.slot
    shift_date = s.date

    shift_date_str = request.POST.get("shiftDate")
    if shift_date_str:
        try:
            shift_date = datetime.datetime.strptime(shift_date_str, SIMPLE_DATE).date()
        except ValueError:
            messages.error(request, "Invalid date format for shift date.")
            return HttpResponseRedirect(reverse("shifter:index"))

    shift_slot_id = request.POST.get("shiftSlot")
    if shift_slot_id:
        try:
            shift_slot = Slot.objects.get(id=int(shift_slot_id))
        except (ValueError, Slot.DoesNotExist):
            messages.error(request, "Invalid slot selected.")
            return HttpResponseRedirect(reverse("shifter:index"))

    if s.member == m and s.date == shift_date and s.slot == shift_slot:
        messages.info(request, "No change applied, no update performed!")
        return HttpResponseRedirect(reverse("shifter:index"))

    if s.member == m:  # if same member, it is just an update
        a = ShiftPlaceHolder()
        a.date = s.date
        a.slot = s.slot

        s.slot = shift_slot
        s.date = shift_date
        s.save()
        notificationService.notify(s, status="update", old_shift=a)

    else:  # member is changing, hence an exchange should be generated
        sExDone = perform_simplified_exchange_and_save_backup(
            shift=s, newMember=m, requestor=request.user, approver=request.user, revisionBackup=get_default_backup_revision()
        )
        notificationService.notify(sExDone)

    messages.success(request, "Requested shift update (member or slot or date) is now successfully implemented.")
    return HttpResponseRedirect(reverse("shifter:index"))


@require_safe
@login_required
def shifts_upload(request):
    data = {
        "campaigns": Campaign.objects.all(),
        "revisions": Revision.objects.filter(merged=False),
        "roles": ShiftRole.objects.all(),
    }
    return render(request, "shifts_upload.html", prepare_default_context(request, data))


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shifts_upload_post(request):
    notifyAfterImport = request.POST.get("make_for_preview", False)
    if int(request.POST["revision"]) < 1:
        revision = Revision(name=request.POST["new-revision-name"], valid=False, date_start=timezone.now())
        revision.save()
    else:
        revision = Revision.objects.filter(number=request.POST["revision"]).first()
    campaign = Campaign.objects.filter(id=request.POST["camp"]).first()
    defaultShiftRole = None
    if int(request.POST["role"]) > 0:
        defaultShiftRole = ShiftRole.objects.filter(id=request.POST["role"]).first()

    importResult = importShiftsFromCSV(request, defaultShiftRole, campaign, revision)
    if importResult.uploadSuccessful:
        if notifyAfterImport:
            revision.ready_for_preview = True
            revision.save()

        for member in set(importResult.affectedMembers):
            newShifts = Shift.objects.filter(member=member, csv_upload_tag=importResult.tagName)
            dailyViolations = find_daily_rest_time_violation(scheduled_shifts=newShifts)
            weeklyViolations = find_weekly_rest_time_violation(scheduled_shifts=newShifts)
            if len(dailyViolations) or len(weeklyViolations):
                messages.warning(
                    request,
                    "There is an inconsistency detected with uploaded shifts for "
                    + str(member)
                    + ". Visit [ShiftInconsistencies Tab in the RotaMaker view] for the details for the revision:"
                    + str(revision),
                )

        messages.success(request, "Uploaded and saved {} shifts provided with {}".format(importResult.totalLinesAdded, importResult.tagName))
    return HttpResponseRedirect(reverse("shifter:shift-upload"))


@require_safe
def phonebook(request):
    context = {
        "result": [],
    }
    return render(request, "phonebook.html", prepare_default_context(request, context))


@require_http_methods(["POST"])
@csrf_protect
def phonebook_post(request):
    tmp = []
    import members.directory as directory

    ldap = directory.LDAP()
    searchKey = request.POST.get("searchKey", "SomeNonsenseToNotBeFound")
    r = ldap.search(field="name", text=searchKey)
    for one in r.keys():
        photo = None
        if len(r[one]["photo"]):
            import base64

            photo = base64.b64encode(r[one]["photo"]).decode("utf-8")
        phoneNb = "N/A"
        try:
            pn = phonenumbers.parse(r[one]["mobile"])
            phoneNb = phonenumbers.format_number(pn, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
        except Exception:
            pass
        tmp.append({"name": one, "mobile": phoneNb, "email": r[one]["email"], "photo": photo, "valid": False})

    context = {"result": [], "searchkey": searchKey}
    invalid = []
    for one in tmp:
        if one["mobile"] == "N/A" or len(one["email"]) == 0:
            invalid.append(one)
            continue
        one["valid"] = True
        context["result"].append(one)
    for one in invalid:
        context["result"].append(one)
    return render(request, "phonebook.html", prepare_default_context(request, context))


class AssetsView(View):
    def get(self, request):
        form = AssetBookingForm(
            {
                "member": request.user,
                "use_start": datetime.datetime.now().strftime(DATE_FORMAT_FULL),
                "use_end": datetime.datetime.now().strftime(DATE_FORMAT_FULL),
            },
            user=request.user,
        )

        closing_form = AssetBookingFormClosing()
        return render(request, "assets.html", {"form": form, "closing_form": closing_form})

    def post(self, request):
        form = AssetBookingForm(request.POST, user=request.user)
        if form.is_valid():
            post = form.save(commit=False)
            post.booking_created = timezone.localtime(timezone.now())
            post.booked_by = request.user
            post.save()
            message = "Booking for {} on {}, is added!".format(post.member.first_name, post.use_start)
            messages.success(request, message)
        else:
            message = "Booking form is not valid, please correct."
            messages.success(request, message)
        return redirect("assets")


@require_http_methods(["POST"])
def assets_close(request):
    booking_id = request.POST.get("booking_id")
    form = AssetBookingFormClosing(request.POST)
    if form.is_valid():
        comment = form.cleaned_data["after_comment"]
        current_booking = get_object_or_404(AssetBooking, id=booking_id)
        current_booking.booking_finished = timezone.localtime(timezone.now())
        current_booking.after_comment = comment
        current_booking.state = AssetBooking.BookingState.RETURNED
        current_booking.finished_by = request.user
        current_booking.save()
        message = "Booking for {} on {}, is now closed!".format(current_booking.member.first_name, current_booking.use_start)
        messages.success(request, message)
    else:
        message = "Booking form is not valid, please correct."
        messages.success(request, message)
    return redirect("assets")


@require_http_methods(["POST"])
@csrf_protect
@login_required
def shift_create(request):
    if request.method == "POST":
        try:
            campaign = Campaign.objects.get(id=request.POST["campaign"])
            date = request.POST["date"]
            slot = Slot.objects.get(id=request.POST["slot"])
            member = Member.objects.get(id=request.POST["member"])
            role = ShiftRole.objects.get(id=request.POST["role"]) if request.POST.get("role") else None
            revision = Revision.objects.get(number=request.POST["revision"])

            Shift.objects.create(
                campaign=campaign,
                date=date,
                slot=slot,
                member=member,
                revision=revision,
                role=role,
            )

            messages.success(request, "Shift created successfully.")

            return redirect(request.META.get("HTTP_REFERER", "/"))
        except Exception as e:
            messages.error(request, f"Error creating shift: {str(e)}")

            return redirect(request.META.get("HTTP_REFERER", "/"))
    else:
        return redirect("/")
