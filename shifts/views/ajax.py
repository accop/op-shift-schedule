from django.http import HttpResponse, JsonResponse
from django.http import HttpRequest
from django.db.models import Count, Q
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_safe

from shifts.exchanges import is_valid_for_hours_constraints_from_market
from shifts.io import write_csv
from shifts.models import *
from studies.models import *
import datetime
from members.models import Team
from assets.models import AssetBooking
from shifts.hrcodes import get_public_holidays, MAX_HOURS_SCHEDULED_PER_CYCLE
from shifts.hrcodes import OB1, OB2, OB3, OB4, NWH, LC1, LC2, ELC, BTP_OR_BTB, OTT_OR_OTW, OB1_WEIGHT, OB2_WEIGHT, OB3_WEIGHT, OB4_WEIGHT
from shifts.models import SIMPLE_DATE
from shifts.hrcodes import get_date_code_counts
from shifter.settings import DEFAULT_SPECIAL_SHIFT_ROLES, DEFAULT_SHIFT_SLOT
from shifts.workinghours import find_daily_rest_time_violation, find_weekly_rest_time_violation, get_hours_break, get_days_of_week, last_day_of_month
import json
from watson import search as watson
from guardian.shortcuts import get_objects_for_user
from django.core.exceptions import PermissionDenied
from shifts.workinghours import get_working_hours as get_working_hours_yearly


def _get_member(request: HttpRequest):
    member_id = request.GET.get("member", -1)
    if member_id == -1:
        member = request.user
    else:
        member = Member.objects.filter(id=member_id).first()
    return member


def _get_revision(request: HttpRequest):
    revision = int(request.GET.get("revision", -1))
    if revision == -1:
        revision = Revision.objects.filter(valid=True).order_by("-number").first()
    else:
        revision = Revision.objects.get(number=revision)
    return revision


def _get_scheduled_shifts(request: HttpRequest):
    start_date = request.GET.get("start", None)
    end_date = request.GET.get("end", None)
    if start_date is None or end_date is None:
        return HttpResponse({}, content_type="application/json", status=500)
    start = datetime.datetime.fromisoformat(start_date).date() - datetime.timedelta(days=1)
    end = datetime.datetime.fromisoformat(end_date).date() + datetime.timedelta(days=1)
    filter_dic = {
        "revision": Revision.objects.filter(valid=True).order_by("-number").first(),
        "date__lte": end,
        "date__gte": start,
        "member": _get_member(request),
    }
    return Shift.objects.filter(**filter_dic)


def _get_companions_shift(member: Member, base_scheduled_shifts):
    filteredCompanions = []
    filter_dic = {
        "date__in": [s.date for s in base_scheduled_shifts],
        "revision__in": [s.revision for s in base_scheduled_shifts],
        "role": None,
    }
    future_companion_shifts = Shift.objects.filter(**filter_dic).filter(~Q(member=member))
    future_companion_exact_shifts = [d.start for d in base_scheduled_shifts]
    for d in future_companion_shifts:
        if d.start in future_companion_exact_shifts:
            filteredCompanions.append(d)
    return filteredCompanions


def _trim4vis(item, default=""):
    return item if item > 0 else default


@require_safe
@login_required
def get_user_events(request: HttpRequest) -> HttpResponse:
    base_scheduled_shifts = _get_scheduled_shifts(request)
    calendar_events = [d.get_shift_as_json_event() for d in base_scheduled_shifts]
    revisionNext = request.GET.get("revision_next", default="-1")
    revisionNext = int(revisionNext)
    show = request.GET.get("show", None)
    show = False if show == "false" else True
    if revisionNext != -1:
        revisionNext = Revision.objects.get(number=revisionNext)
        future_scheduled_shifts = Shift.objects.filter(member=_get_member(request), revision=revisionNext)
        for d in future_scheduled_shifts:
            calendar_events.append(d.get_planned_shift_as_json_event())
    withCompanion = request.GET.get("companion", default=None)
    withCompanion = True if withCompanion == "true" else False
    if withCompanion:
        for d in _get_companions_shift(_get_member(request), base_scheduled_shifts):
            calendar_events.append(d.get_shift_as_json_event())
    if not show:
        calendar_events = []
    return HttpResponse(json.dumps(calendar_events), content_type="application/json")


@require_safe
@login_required
def get_users_events(request: HttpRequest) -> HttpResponse:
    users_requested = request.GET.get("users", "")
    users_requested = users_requested.split(",")
    users_requested = [int(x) for x in users_requested] if users_requested != [""] else []

    start_date = request.GET.get("start", None)
    end_date = request.GET.get("end", None)
    all_roles = request.GET.get("all_roles", None)
    all_states = request.GET.get("all_states", None)
    campaigns = request.GET.get("campaigns", None)
    revision = _get_revision(request)
    revisionNext = request.GET.get("revision_next", default="-1")

    if start_date is None or end_date is None or all_roles is None or campaigns is None or revision is None:
        return HttpResponse({}, content_type="application/json", status=500)

    campaigns = campaigns.split(",")
    campaigns = [int(x) for x in campaigns] if campaigns != [""] else []
    all_roles = True if all_roles == "true" else False
    all_states = True if all_states == "true" else False
    start = datetime.datetime.fromisoformat(start_date).date() - datetime.timedelta(days=1)
    end = datetime.datetime.fromisoformat(end_date).date() + datetime.timedelta(days=1)
    scheduled_campaigns = Campaign.objects.filter(revision=revision).filter(id__in=campaigns)

    filter_dic = {
        "date__gt": start,
        "date__lt": end,
        "revision": revision,
        "campaign__in": scheduled_campaigns,
        "member_id__in": users_requested,
        "is_cancelled": False,
        "is_active": True,
    }

    isRotaMaker = request.user.team in get_objects_for_user(request.user, "members.view_desiderata")
    if not isRotaMaker:
        filter_dic["is_cancelled"] = False
        filter_dic["is_active"] = True

    if all_states:
        filter_dic.pop("is_cancelled")
        filter_dic.pop("is_active")

    scheduled_shifts = Shift.objects.filter(**filter_dic).order_by("date", "slot__hour_start", "member__role__priority")
    if not all_roles:
        scheduled_shifts = scheduled_shifts.filter(
            Q(role=None) | Q(role__in=[one for one in ShiftRole.objects.filter(abbreviation__in=DEFAULT_SPECIAL_SHIFT_ROLES)])
        )
    calendar_events = [d.get_shift_as_json_event() for d in scheduled_shifts]

    revisionNext = int(revisionNext)
    if revisionNext != -1:
        revisionNext = Revision.objects.get(number=revisionNext)
        future_scheduled_shifts = Shift.objects.filter(member_id__in=users_requested, revision=revisionNext)
        for d in future_scheduled_shifts:
            calendar_events.append(d.get_planned_shift_as_json_event())

    return HttpResponse(json.dumps(calendar_events), content_type="application/json")


@require_safe
def get_events(request: HttpRequest) -> HttpResponse:
    start_date = request.GET.get("start", None)
    end_date = request.GET.get("end", None)
    all_roles = request.GET.get("all_roles", None)
    campaigns = request.GET.get("campaigns", None)
    revision = request.GET.get("revision", None)
    team_id = request.GET.get("team", -1)

    if start_date is None or end_date is None or all_roles is None or campaigns is None or revision is None:
        return HttpResponse({}, content_type="application/json", status=500)

    campaigns = campaigns.split(",")
    campaigns = [int(x) for x in campaigns] if campaigns != [""] else []
    all_roles = True if all_roles == "true" else False
    start = datetime.datetime.fromisoformat(start_date).date() - datetime.timedelta(days=1)
    end = datetime.datetime.fromisoformat(end_date).date() + datetime.timedelta(days=1)
    revision = int(revision)

    if revision == -1:
        revision = Revision.objects.filter(valid=True).order_by("-number").first()
    else:
        revision = Revision.objects.get(valid=True, number=revision)
    scheduled_campaigns = Campaign.objects.filter(revision=revision).filter(id__in=campaigns)

    filter_dic = {
        "date__gt": start,
        "date__lt": end,
        "revision": revision,
        "campaign__in": scheduled_campaigns,
    }
    isRotaMaker = False
    if request.user.is_authenticated:
        isRotaMaker = request.user.team in get_objects_for_user(request.user, "members.view_desiderata")
    if not isRotaMaker:
        filter_dic["is_cancelled"] = False
        filter_dic["is_active"] = True

    if int(team_id) > 0:
        team = Team.objects.get(id=team_id)
        filter_dic["member__team"] = team
    scheduled_shifts = Shift.objects.filter(**filter_dic).order_by("date", "slot__hour_start", "member__role__priority")
    if not all_roles:
        scheduled_shifts = scheduled_shifts.filter(
            Q(role=None) | Q(role__in=[one for one in ShiftRole.objects.filter(abbreviation__in=DEFAULT_SPECIAL_SHIFT_ROLES)])
        )

    calendar_events = [d.get_shift_as_json_event() for d in scheduled_shifts]

    return HttpResponse(json.dumps(calendar_events), content_type="application/json")


@require_safe
def get_team_events(request: HttpRequest) -> HttpResponse:
    # TODO Remove the hardcoded dates here BUT remember!
    # TODO make this Model based AFTER other branches are merged - to avoid DB model conflicts.
    days = get_days_of_week(17, year=2024) + get_days_of_week(20, year=2024) + get_days_of_week(21, year=2024) + get_days_of_week(35, year=2024)
    calendar_events = [
        {"start": d.strftime(format=SIMPLE_DATE), "end": d.strftime(format=SIMPLE_DATE), "overlap": True, "color": "#FFE7CB", "display": "background"}
        for d in days
    ]
    return HttpResponse(json.dumps(calendar_events), content_type="application/json")


@require_safe
def get_holidays(request: HttpRequest) -> HttpResponse:
    start = datetime.datetime.fromisoformat(request.GET.get("start")).date()
    end = datetime.datetime.fromisoformat(request.GET.get("end")).date()
    ph = [x for x in get_public_holidays() if start <= x <= end]
    calendar_events = [
        {"start": d.strftime(format=SIMPLE_DATE), "end": d.strftime(format=SIMPLE_DATE), "overlap": True, "color": "#8fdf82", "display": "background"}
        for d in ph
    ]
    return HttpResponse(json.dumps(calendar_events), content_type="application/json")


@require_safe
def get_assets(request: HttpRequest) -> HttpResponse:
    bookings = AssetBooking.objects.all().order_by("-use_start")
    data = {"data": [b.asset_as_json(user=request.user) for b in bookings]}
    return JsonResponse(data)


@require_safe
@login_required()
def get_hr_codes(request: HttpRequest) -> HttpResponse:
    member = request.user

    default_start = datetime.datetime.strptime(request.GET.get("start", "2022/01/01"), "%Y-%m-%d").date()
    default_end = datetime.datetime.strptime(request.GET.get("end", "2022/01/01"), "%Y-%m-%d").date()

    data, smalldata, mod = genarate_codes(member, default_start, default_end)

    return HttpResponse(json.dumps({"data": data, "dataSmall": list(map(list, zip(*smalldata))), "dataModifier": mod}), content_type="application/json")


def _getFromCode(item):
    return item[OB1] * OB1_WEIGHT + item[OB2] * OB2_WEIGHT + item[OB3] * OB3_WEIGHT + item[OB4] * OB4_WEIGHT


def genarate_codes(member, default_start, default_end):
    revision = Revision.objects.filter(valid=True).order_by("-number").first()

    filter_dic = {"date__gte": default_start, "date__lte": default_end, "revision": revision, "member": member, "is_active": True}
    filter_vac = {"date__gte": default_start, "date__lte": default_end, "revision": revision, "member": member, "is_vacation": True}

    scheduled_shifts = Shift.objects.filter(**filter_dic).order_by("-date")
    vacation_shifts = Shift.objects.filter(**filter_vac).order_by("-date")
    dates = [v.date.strftime(SIMPLE_DATE) for v in vacation_shifts]

    shift2codes = get_date_code_counts(scheduled_shifts)

    def _check(date):
        if date in dates:
            return "1"
        return ""

    data = []
    modifier = 0.0
    smalldata = []
    for date, item in shift2codes.items():
        a_row = [
            date[2:],
            "",  # vacation comes in the other line
            _trim4vis(item[NWH]),
            _trim4vis(item[OB1]),
            _trim4vis(item[OB2]),
            _trim4vis(item[OB3]),
            _trim4vis(item[OB4]),
            _trim4vis(item[BTP_OR_BTB]),
            _trim4vis(item[ELC]),
            _trim4vis(item[LC1]),
            _trim4vis(item[LC2]),
            _trim4vis(item[OTT_OR_OTW]),
        ]
        data.append(a_row)
        modifier += _getFromCode(item)
        a_row = [
            date[2:],
            _trim4vis(item[NWH]),
            _trim4vis(item[OB1]),
            _trim4vis(item[OB2]),
            _trim4vis(item[OB3]),
            _trim4vis(item[OB4]),
        ]
        smalldata.append(a_row)

    for date in dates:
        a_row = [
            date[2:],
            "1",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        ]
        data.append(a_row)

    for date in dates:
        a_row = [
            date[2:],
            "",
            "",
            "",
            "",
            "",
        ]
        smalldata.append(a_row)
    return data, smalldata, modifier


@require_safe
@login_required()
def get_working_hours(request: HttpRequest) -> HttpResponse:
    member = request.user
    default_start_str = request.GET.get("start", datetime.date.today().strftime(SIMPLE_DATE))
    default_start = datetime.datetime.strptime(default_start_str, SIMPLE_DATE).date()
    duration = int(request.GET.get("duration"))
    if duration == 1:
        duration = last_day_of_month(datetime.datetime.combine(default_start, datetime.time())).day
    default_end = default_start + datetime.timedelta(days=duration - 1)

    revision = Revision.objects.filter(valid=True).order_by("-number").first()
    filterDict = {"date__gte": default_start, "date__lte": default_end, "revision": revision}

    response_data = get_working_hours_yearly(filterDict, [member])[0]
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@require_safe
@login_required()
def get_team_hr_codes(request: HttpRequest) -> HttpResponse:
    logged_user = request.user
    logged_user_manage = get_objects_for_user(logged_user, "members.view_desiderata")

    default_start = datetime.datetime.strptime(request.GET.get("start", "2022/01/01"), "%Y-%m-%d").date()
    default_end = datetime.datetime.strptime(request.GET.get("end", "2022/01/01"), "%Y-%m-%d").date()
    team = request.GET.get("team", -1)
    team = Team.objects.get(id=team)
    if team not in logged_user_manage:
        raise PermissionDenied
    team_members = Member.objects.filter(team=team).filter(is_active=True)

    revision = Revision.objects.filter(valid=True).order_by("-number").first()
    data = []

    for m in team_members:
        filter_dic = {"date__gte": default_start, "date__lte": default_end, "revision": revision, "member": m, "is_active": True}
        filter_vac = {"date__gte": default_start, "date__lte": default_end, "revision": revision, "member": m, "is_vacation": True}

        scheduled_shifts = Shift.objects.filter(**filter_dic).order_by("-date")
        vacation_shifts = Shift.objects.filter(**filter_vac).order_by("-date")
        dates = [v.date.strftime(SIMPLE_DATE) for v in vacation_shifts]

        shift2codes = get_date_code_counts(scheduled_shifts)

        for date, item in shift2codes.items():
            a_row = [
                date[2:],
                str(m),
                "",  # vacation comes in the other line
                _trim4vis(item[NWH]),
                _trim4vis(item[OB1]),
                _trim4vis(item[OB2]),
                _trim4vis(item[OB3]),
                _trim4vis(item[OB4]),
                _trim4vis(item[BTP_OR_BTB]),
                _trim4vis(item[ELC]),
                _trim4vis(item[LC1]),
                _trim4vis(item[LC2]),
                _trim4vis(item[OTT_OR_OTW]),
            ]
            data.append(a_row)

        for date in dates:
            a_row = [
                date[2:],
                str(m),
                "1",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ]
            data.append(a_row)

    return HttpResponse(json.dumps({"data": data}), content_type="application/json")


@require_safe
@login_required()
def get_team_shiftControl(request: HttpRequest) -> HttpResponse:
    start_date = request.GET.get("start", None)
    end_date = request.GET.get("end", None)
    team_id = request.GET.get("team", -1)
    team_id = int(team_id)
    revision = _get_revision(request)

    start = datetime.datetime.fromisoformat(start_date).date()
    end = datetime.datetime.fromisoformat(end_date).date()

    shifts4Control = (
        Shift.objects.filter(revision=revision)
        .filter(date__gte=start)
        .filter(date__lte=end)
        .filter(is_active=True)
        .filter(is_cancelled=False)
        .order_by("date", "slot__hour_start")
    )

    shifts_by_day = {}

    for shift in shifts4Control:
        if shift.role is not None:
            continue

        date_str = f"{shift.date} ({shift.slot.name})"

        if date_str not in shifts_by_day:
            shifts_by_day[date_str] = {"members": [], "roles": {"ShiftLeader": False, "Operator": False}}

        shifts_by_day[date_str]["members"].append(f"{shift.member.first_name} {shift.member.last_name} ({shift.member.role})")

        if "ShiftLeader" in shift.member.role.name:
            shifts_by_day[date_str]["roles"]["ShiftLeader"] = True
        if "Operator" in shift.member.role.name:
            shifts_by_day[date_str]["roles"]["Operator"] = True

    filtered_shifts = {date: info for date, info in shifts_by_day.items() if not (info["roles"]["ShiftLeader"] and info["roles"]["Operator"])}

    dataToReturn = [{"date": date, "members": ", ".join(info["members"])} for date, info in filtered_shifts.items()]

    response_data = {
        "data": dataToReturn,
        "header": f'Shifts control from {start.strftime("%A, %B %d, %Y")} to {(end - datetime.timedelta(days=1)).strftime("%A, %B %d, %Y")}',
        "date-start": start.strftime("%Y-%m-%d"),
        "date-end": end.strftime("%Y-%m-%d"),
        "revision": str(revision),
    }

    return HttpResponse(json.dumps(response_data), content_type="application/json")


def get_shift_summary(m, validSlots, revision, start, end) -> tuple:
    scheduled_shifts = Shift.objects.filter(member=m, slot__in=validSlots, revision=revision, date__gte=start, date__lt=end).count()

    differentSlots = (
        Shift.objects.filter(member=m, slot__in=validSlots, revision=revision, date__gte=start, date__lt=end)
        .values("slot__abbreviation")
        .annotate(total=Count("slot"))
    )

    result = {a["slot__abbreviation"]: a["total"] for a in differentSlots}
    return scheduled_shifts, result


@login_required
def get_shift_breakdown(request: HttpRequest) -> HttpResponse:
    start = datetime.datetime.fromisoformat(request.GET.get("start")).date()
    end = datetime.datetime.fromisoformat(request.GET.get("end")).date()
    revision = _get_revision(request)
    team = request.user.team

    teamMembers = Member.objects.filter(team=team, is_active=True)
    validSlots = Slot.objects.filter(used_for_lookup=True).order_by("hour_start")
    teamMembersSummary = []
    for m in teamMembers:
        l, result = get_shift_summary(m, validSlots, revision, start, end)
        memberSummary = [m.name, l]
        for oneSlot in validSlots:
            memberSummary.append(result.get(oneSlot.abbreviation, "--"))
        if memberSummary[1] > 0:
            teamMembersSummary.append(memberSummary)

    header = f'Showing shift breakdown from {start.strftime("%A, %B %d, %Y ")} to {(end - datetime.timedelta(days=1)).strftime("%A, %B %d, %Y ")}'

    return HttpResponse(
        json.dumps(
            {
                "data": teamMembersSummary,
                "header": header,
                "date-start": start.strftime(DATE_FORMAT_FULL),
                "date-end": end.strftime(DATE_FORMAT_FULL),
                "slots": [one.__str__() for one in validSlots],
                "userdata": [{} for one in teamMembersSummary],
                "revision": revision.__str__(),
            }
        ),
        content_type="application/json",
    )


@login_required
def get_market_events(request: HttpRequest) -> HttpResponse:
    shifts_on_market = ShiftForMarket.objects.filter(is_available=True, shift__member__role=request.user.role)
    calendar_market_events = [d.get_market_shift_as_json_event() for d in shifts_on_market]
    return HttpResponse(json.dumps(calendar_market_events), content_type="application/json")


@login_required
def get_market_validity(request: HttpRequest) -> HttpResponse:
    revision = _get_revision(request)
    try:
        sfm = ShiftForMarket.objects.get(id=request.GET.get("market_id"))
    except:
        return HttpResponse(json.dumps({"applicable": False, "info": "Wrong ID"}), content_type="application/json")

    is_applicable = is_valid_for_hours_constraints_from_market(sfm, revision, request.user)
    return HttpResponse(
        json.dumps(
            {
                "applicable": is_applicable[0],
                "daily_violation": len(is_applicable[1][0]),
                "weekly_violation": len(is_applicable[1][1]),
                "hours": is_applicable[1][2],
                "hoursOld": is_applicable[1][3],
                "reference_hours": MAX_HOURS_SCHEDULED_PER_CYCLE,
            }
        ),
        content_type="application/json",
    )


@login_required
def get_shifts_for_exchange(request: HttpRequest) -> HttpResponse:
    member = _get_member(request)
    revision = _get_revision(request)
    toReturn = []
    if request.GET.get("option", "my") == "my":
        futureMy = Shift.objects.filter(revision=revision, member=member, date__gte=timezone.now()).order_by("date")
        toReturn = [s.get_simplified_as_json() for s in futureMy]
    if request.GET.get("option", "my") == "them":
        futureOther = Shift.objects.filter(~Q(member=member), revision=revision, date__gte=timezone.now()).order_by("date")
        toReturn = [s.get_simplified_as_json() for s in futureOther]
    return HttpResponse(json.dumps(toReturn), content_type="application/json")


def _get_inconsistencies_per_member(member, revision):
    # TODO fix it with proper js file to build it out of JSON
    # TODO once with json, return count of inconsistencies to enable badge
    ss = Shift.objects.filter(revision=revision, is_active=True).filter(member=member)
    dailyViolations = find_daily_rest_time_violation(scheduled_shifts=ss)
    weeklyViolations = find_weekly_rest_time_violation(scheduled_shifts=ss)
    toReturnHTML = ""
    if len(dailyViolations):
        toReturnHTML += '<dl class="row">' '<dt class="col-sm-3">Daily shift issues:</dt>' '<dd class="col-sm-9">'
        for oneD in dailyViolations:
            label = "light"
            if oneD[0].start > datetime.datetime.now():
                label = "danger"
            toReturnHTML += (
                f'<p><span class="badge text-bg-{label}">{oneD[0].get_shift_as_date_slot()}</span> - '
                f'<span class="badge text-bg-{label}">{oneD[1].get_shift_as_date_slot()} </span> '
                f'<span class="badge text-bg-dark">{get_hours_break(oneD[1], oneD[0])}h</span></p>'
            )
        toReturnHTML += "</dd></dl>"
    if len(weeklyViolations):
        toReturnHTML += '<dl class="row">' '<dt class="col-sm-3">Weekly shift issues:</dt>' '<dd class="col-sm-9">'
        for oneW in weeklyViolations:
            for i, oneInW in enumerate(oneW):
                label = "light"
                if oneInW.start > datetime.datetime.now():
                    label = "danger"
                breakLength = ""
                if i < len(oneW) - 1:
                    breakLength = f'<span class="badge text-bg-dark">{get_hours_break(oneW[i + 1], oneInW)}h</span>'
                toReturnHTML += f'<p><span class="badge text-bg-{label}">{oneInW.get_shift_as_date_slot()}</span>{breakLength}</p>'
            toReturnHTML += "<hr>"
        toReturnHTML += "</dd></dl>"
    if len(dailyViolations) or len(weeklyViolations):
        return toReturnHTML
    else:
        return None


@login_required
def get_applicability_for_change(request: HttpRequest):
    member = Member.objects.get(id=request.GET.get("member"))
    slot = Slot.objects.get(id=request.GET.get("slot"))
    oldShift = Shift.objects.get(id=request.GET.get("oldShift"))
    date = datetime.datetime.fromisoformat(request.GET.get("date"))
    default_start = date + datetime.timedelta(days=-10)
    default_end = date + datetime.timedelta(days=10)

    baseMembersShifts = Shift.objects.filter(revision=_get_revision(request), is_active=True).filter(
        member=member,
        date__gte=default_start,
        date__lte=default_end,
    )

    from shifts.exchanges import _create_shift

    fakeNonPersistedNew = _create_shift(oldShift, member, permanent=False)
    shiftsAfterChange = list(baseMembersShifts)
    note = "Not Applicable"
    if oldShift.member != member:
        specialCase = Shift.objects.filter(member=member, date=oldShift.date, slot=Slot.objects.get(abbreviation=DEFAULT_SHIFT_SLOT))
        if len(specialCase):
            if specialCase.first() in shiftsAfterChange:
                shiftsAfterChange.remove(specialCase.first())
                note += "<br>Note: " + member.first_name + " NWH on (" + date.date() + ") shift removed, from the check!"
        shiftsAfterChange.append(fakeNonPersistedNew)

    if oldShift.slot != slot:
        if oldShift in shiftsAfterChange:
            shiftsAfterChange.remove(oldShift)
        shiftsAfterChange.append(_create_shift(oldShift, oldShift.member, slot=slot))
    if oldShift.date != date.date():
        if oldShift in shiftsAfterChange:
            shiftsAfterChange.remove(oldShift)
        shiftsAfterChange.append(_create_shift(oldShift, oldShift.member, date=date.date()))

    dailyViolations = find_daily_rest_time_violation(scheduled_shifts=shiftsAfterChange)
    weeklyViolations = find_weekly_rest_time_violation(scheduled_shifts=shiftsAfterChange)

    if len(dailyViolations):
        note += "<br> Breaking " + member.first_name + "'s daily rest limit."
    if len(weeklyViolations):
        note += "<br> Breaking " + member.first_name + "'s 10days rest limit."

    applicable = True
    if len(dailyViolations) or len(weeklyViolations):
        applicable = False
    return JsonResponse(
        {
            "applicable": applicable,
            "note": note,
        }
    )


@login_required
def get_shift_inconsistencies(request: HttpRequest) -> HttpResponse:
    revision = _get_revision(request)
    toReturnHTML = _get_inconsistencies_per_member(request.user, revision)
    return HttpResponse(toReturnHTML, content_type="application/text")


@login_required
def get_team_shift_inconsistencies(request: HttpRequest) -> HttpResponse:
    revision = _get_revision(request)
    team = request.user.team
    teamId = int(request.GET.get("tid", -1))
    if teamId > 0:
        team = Team.objects.get(id=teamId)
    members = Member.objects.filter(team=team, is_active=True)
    # TODO improve when _get_per_member's TODOs solved
    toReturnHTML = ""
    for member in members:
        foundInconsistencies = _get_inconsistencies_per_member(member, revision)
        if foundInconsistencies is not None:
            toReturnHTML += '<hr><h5 class="mb-3">{}</h5>'.format(member)
            toReturnHTML += foundInconsistencies
    return HttpResponse(toReturnHTML, content_type="application/text")


@login_required
def get_shift_stats(request: HttpRequest) -> HttpResponse:
    start_date = request.GET.get("start", None)
    end_date = request.GET.get("end", None)
    statType = request.GET.get("statType", None)
    teamId = request.GET.get("teamId", -1)
    teamId = int(teamId)
    revision = _get_revision(request)
    start = datetime.datetime.fromisoformat(start_date).date()
    end = datetime.datetime.fromisoformat(end_date).date()
    shifts4Stat = Shift.objects.filter(revision=revision).filter(member__team__id=teamId).filter(date__gte=start).filter(date__lte=end).order_by("date")
    dataToReturn = []
    if statType == "workWith":
        # FIXME there is no way to have distinct in sqllite hence the next few lines
        differentMembers = []
        differentShiftStarts = []
        for s in shifts4Stat:
            differentShiftStarts.append(s.start)
            differentMembers.append(s.member)
        differentMembers = list(set(differentMembers))
        differentShiftStarts = list(set(differentShiftStarts))
        differentShiftStarts.sort()
        # prepare the counter
        dataCount = {}
        for dm in differentMembers:
            dataCount[dm] = {}
            for dmX in differentMembers:
                if dmX == dm:
                    continue
                dataCount[dm][dmX] = 0
        # count
        for oneShiftStart in differentShiftStarts:
            temp = []
            # find same shifts (date + slot)
            for oneShift in shifts4Stat:
                if oneShift.start == oneShiftStart:
                    temp.append(oneShift)
            # count the encounters
            for one in temp:
                for second in temp:
                    if one.member == second.member:
                        continue
                    dataCount[one.member][second.member] += 1
        dataToReturn = []
        for one in dataCount.keys():
            for second in dataCount[one]:
                if dataCount[one][second] != 0:
                    dataToReturn.append([one.first_name, second.first_name, dataCount[one][second]])
    header = f'Showing shift companions from {start.strftime("%A, %B %d, %Y ")} to {(end - datetime.timedelta(days=1)).strftime("%A, %B %d, %Y ")}'
    return HttpResponse(
        json.dumps(
            {
                "data": dataToReturn,
                "header": header,
                "date-start": start.strftime(DATE_FORMAT_FULL),
                "date-end": end.strftime(DATE_FORMAT_FULL),
                "revision": revision.__str__(),
            }
        ),
        content_type="application/json",
    )


@login_required
def get_team_shiftswaps(request: HttpRequest) -> HttpResponse:
    start = datetime.datetime.fromisoformat(request.GET.get("start")).date()
    end = datetime.datetime.fromisoformat(request.GET.get("end")).date()
    team = request.user.team
    sExs = ShiftExchange.objects.filter(implemented=True).filter(
        (Q(requestor__team=team) | Q(shifts__shift_for_exchange__member__team=team) | Q(shifts__shift__member__team=team))
        & (
            Q(shifts__shift__date__gte=start)
            & Q(shifts__shift__date__lte=end)
            & Q(shifts__shift_for_exchange__date__gte=start)
            & Q(shifts__shift_for_exchange__date__lte=end)
        )
    )
    ids = []
    data = []
    for onesEx in sExs:
        if onesEx.id in ids:
            continue  # poor's man remove duplicates
        ids.append(onesEx.id)
        swaps = ""
        for oneSwap in onesEx.shifts.all():
            swaps += str(oneSwap) + "<br>"
        data.append(
            [
                '<a class="link" href="' + reverse("shifter:shift-exchange-view", kwargs={"ex_id": onesEx.id}) + '">' + str(onesEx.id) + "</a>",
                onesEx.type,
                onesEx.requested_date,
                onesEx.requestor.name,
                onesEx.approved_date,
                onesEx.approver.name,
                swaps,
            ]
        )

    return HttpResponse(
        json.dumps(
            {
                "data": data,
                "header": "Some header",
                "date-start": start.strftime(DATE_FORMAT_FULL),
                "date-end": end.strftime(DATE_FORMAT_FULL),
            }
        ),
        content_type="application/json",
    )


def search(request: HttpRequest) -> HttpResponse:
    answer = []
    search = request.GET.get("search")
    search_results = watson.search(search, ranking=False)
    for result in search_results:
        try:
            answer.append({"value": result.object.search_display(), "url": result.object.search_url()})
        except AttributeError:
            pass
    return HttpResponse(json.dumps(answer), content_type="application/json")


@login_required
def get_events_csv(request: HttpRequest) -> HttpResponse:
    try:
        team_id = int(request.GET.get("team", -1))
        start_date = request.GET.get("start")
        end_date = request.GET.get("end")
        revision = request.GET.get("revision")

        if not all([start_date, end_date, revision]):
            return HttpResponse("Missing parameters.", content_type="text/plain", status=400)

        start, end, revision, team, member = _prepare_data(request, start_date, end_date, revision, team_id)
        filter_dic = _build_filter_dict(start, end, revision, member=member, campaigns=Campaign.objects.filter(revision=revision), team=team)
        scheduled_shifts = Shift.objects.filter(**filter_dic).order_by("date", "slot__hour_start", "member__role__priority", "is_vacation")

        shift_data = _create_shift_data(scheduled_shifts, start, end)

        headers, data = _format_csv_data(shift_data, start, end)
        return write_csv(data, headers, "ManualExport__{}.csv".format(start))

    except ValueError:
        return HttpResponse("Invalid date format.", content_type="text/plain", status=400)


def _prepare_data(request, start_date, end_date, revision, team_id):
    start = parse_date(start_date, with_time=True).replace(hour=0, minute=0, second=0, microsecond=0)
    end = parse_date(end_date, with_time=True).replace(hour=23, minute=59, second=59, microsecond=999999)

    if revision == "-1":
        revision = Revision.objects.filter(valid=True).order_by("-number").first()
    else:
        revision = Revision.objects.get(valid=True, number=int(revision))

    team = Team.objects.get(id=team_id) if team_id > 0 else None
    member = request.user if team_id == -1 else None

    return start, end, revision, team, member


def _create_shift_data(scheduled_shifts, start, end, is_vacation=False):
    shift_data = {}
    for shift in scheduled_shifts:
        name = shift.member.first_name
        date = shift.date
        is_vacation = shift.is_vacation
        vac = ":VAC" if is_vacation else ""
        shift_role = f":{shift.role.abbreviation}" if shift.role else ""
        shift_slot = shift.slot.abbreviation

        if name not in shift_data:
            shift_data[name] = {d: "" for d in (start.date() + datetime.timedelta(days=i) for i in range((end.date() - start.date()).days + 1))}

        shift_data[name][date] = f"{shift_slot}{shift_role}{vac}"

    return shift_data


def _format_csv_data(shift_data, start, end):
    headers = ["# Name"] + [(start.date() + datetime.timedelta(days=i)).strftime("%Y-%m-%d") for i in range((end.date() - start.date()).days + 1)]
    data = [
        [name]
        + [shifts.get(current_date, "") for current_date in (start.date() + datetime.timedelta(days=i) for i in range((end.date() - start.date()).days + 1))]
        for name, shifts in shift_data.items()
    ]
    return headers, data


def parse_date(date_str, default="2022-01-01", with_time=False):
    if with_time:
        return timezone.make_aware(datetime.datetime.fromisoformat(date_str), timezone.get_current_timezone())
    return datetime.datetime.strptime(date_str or default, DATE_FORMAT_SLIM).date()


def _build_filter_dict(start, end, revision, member=None, campaigns=None, team=None):
    # filter_q = Q(is_active=True) | Q(is_active=False, is_vacation=True)

    filter_dic = {
        "date__gte": start,
        "date__lte": end,
        "revision": revision,
    }

    # queryset = Shift.objects.filter(Q(**filter_dic) & filter_q)

    if member:
        filter_dic["member"] = member
    if campaigns:
        filter_dic["campaign__in"] = campaigns
    if team:
        filter_dic["member__team"] = team
    return filter_dic
