from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from shifts.views.main import notificationService
from shifts.models import Revision, Shift

"""
This is needed to handle all 'internal' operations (eg. via admin panel) that should be notifiable
"""


@receiver(pre_save, sender=Shift)
def store_previous_is_cancelled(sender, instance, **kwargs):
    if instance.pk:
        instance._previous_is_cancelled = Shift.objects.get(pk=instance.pk).is_cancelled


@receiver(post_save, sender=Shift)
def notify_user_on_shift_cancelled_change(sender, instance, created, **kwargs):
    if not created and hasattr(instance, "_previous_is_cancelled"):
        if instance._previous_is_cancelled != instance.is_cancelled:
            status = "cancelled" if instance.is_cancelled else "reactivated"
            notificationService.notify(instance, status=status)


@receiver(post_save, sender=Revision)
def notify_at_save(sender, instance, **kwargs):
    # notify only on particular state change
    if instance.ready_for_preview:
        notificationService.notify(instance)
