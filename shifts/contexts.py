from django.db.models import Count
import django.contrib.messages as messages
from members.models import Team
from shifts.models import Campaign, Revision, Shift, Slot, ShifterMessage
from shifts.models import MONTH_NAME, SIMPLE_DATE
from shifts.hrcodes import get_date_code_counts, count_total
import datetime

from shifter.settings import (
    WWW_EXTRA_INFO,
    PHONEBOOK_NAME,
    DEFAULT_SHIFT_SLOT,
)


def get_default_backup_revision():
    #  TODO add some fancy handling if it does not exists
    return Revision.objects.filter(name__startswith="BACKUP").first()


def get_latest_valid_revision():
    return Revision.objects.filter(valid=True).order_by("-number").first()


def prepare_default_context(request, contextToAdd):
    """
    providing any of the following will override their default values:
    @defaultDate   now
    @latest_revision  last created with valid status
    @APP_NAME   name of the APP displayed in the upper left corner
    """
    date = datetime.datetime.now().date()
    latest_revision = get_latest_valid_revision()

    for oneShifterMessages in ShifterMessage.objects.filter(valid=True).order_by("-number"):
        messages.warning(request, oneShifterMessages.description)

    context = {
        "logged_user": request.user.is_authenticated,
        "defaultDate": date.strftime(SIMPLE_DATE),
        "slots": Slot.objects.filter(op=True).order_by("hour_start"),
        "teams": Team.objects.all().order_by("name"),
        "latest_revision": latest_revision,
        "displayed_revision": latest_revision,
        "PHONEBOOK_NAME": PHONEBOOK_NAME,
        "DEFAULT_SHIFT_SLOT": Slot.objects.get(abbreviation=DEFAULT_SHIFT_SLOT),
        "wwwWithMoreInfo": WWW_EXTRA_INFO,
    }
    for one in contextToAdd.keys():
        context[one] = contextToAdd[one]
    return context


def prepare_user_context(member, revisionNext=None):
    currentMonth = datetime.datetime.now()
    nextMonth = currentMonth + datetime.timedelta(30)  # banking rounding
    revision = get_latest_valid_revision()
    newer_revisions = Revision.objects.filter(date_start__gt=revision.date_start).filter(ready_for_preview=True).filter(merged=False).order_by("-number")
    scheduled_shifts = Shift.objects.filter(member=member, revision=revision).order_by("-date")
    # scheduled_studies = StudyRequest.objects.filter(member=member,state__in=["B","D"]).order_by('slot_start', 'priority')
    shift2codes = get_date_code_counts(scheduled_shifts)
    scheduled_campaigns = Campaign.objects.all().filter(revision=revision)
    context = {
        "member": member,
        "currentmonth": currentMonth.strftime(MONTH_NAME),
        "nextmonth": nextMonth.strftime(MONTH_NAME),
        "scheduled_campaigns_list": scheduled_campaigns,
        "newer_revisions": newer_revisions,
        "campaigns": Campaign.objects.filter(revision=revision),
        "hrcodes": shift2codes,
        "hrcodes_summary": count_total(shift2codes),
    }
    return context


def get_shift_summary(m, validSlots, revision, currentMonth) -> tuple:
    scheduled_shifts = Shift.objects.filter(member=m, revision=revision, date__year=currentMonth.year, date__month=currentMonth.month)

    differentSlots = (
        Shift.objects.filter(member=m, revision=revision, date__year=currentMonth.year, date__month=currentMonth.month)
        .values("slot__abbreviation")
        .annotate(total=Count("slot"))
    )

    result = {a["slot__abbreviation"]: a["total"] for a in differentSlots}
    return len(scheduled_shifts), result
