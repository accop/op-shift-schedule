from django import template

register = template.Library()


@register.filter
def ical_escape(text):
    """Replace new lines with \n and ensure end-of-line with \\n."""
    if text:
        text = text.replace("\r\n", "\n")
        text = text.replace("\n", "\\n")
    return text
