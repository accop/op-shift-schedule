from django.db.models import Q
from shifts.hrcodes import MAX_HOURS_SCHEDULED_PER_CYCLE
from shifts.models import ShiftExchange, Member, Shift, ShiftExchangePair, Revision, ShiftForMarket, Slot
from shifts.workinghours import (
    find_daily_rest_time_violation,
    find_weekly_rest_time_violation,
    find_current_demarcation_range,
    get_working_hours,
    get_reportable_shift_duration_h,
)
from datetime import timedelta
from django.utils import timezone


def get_exchange_exchange_preview(shiftExchange: ShiftExchange, baseRevision, previewRangeDays=10, verbose=False):
    """
    Returns all new shift plan wrt shifts requested for change (each change component
    queries previewRangeDays forward and backward)

    All lists wiht new 'after change shifts' is returned.

    WARNING: at this stage, no changes to any shift if provided, the 'new'
    theoretical shifts exists ONLY as objects and are not represented in the db

    """
    if verbose:
        print("\t============================")
        print("\t----- TESTING validity -----")
        print("\t============================")
        print("\t", shiftExchange)

    allShiftsForNewPlanning = []
    usedSlots = []  # these are needed prior to the main check!
    for shiftSwap in shiftExchange.shifts.all():
        usedSlots += [shiftSwap.shift.id, shiftSwap.shift_for_exchange.id]

    for shiftSwap in shiftExchange.shifts.all():
        allShiftsForNewPlanning = _verifyForOneShiftSwap(shiftSwap, usedSlots, baseRevision, previewRangeDays=previewRangeDays, verbose=verbose)

    if verbose:
        print(allShiftsForNewPlanning)
        print(shiftExchange.applicable)
        print("\t============================")
    return allShiftsForNewPlanning


def _verifyForOneShiftSwap(shiftSwap: ShiftExchangePair, usedSlots, baseRevision, previewRangeDays=10, verbose=False):
    """
    Performs a virtual check for the
    """
    allShiftsForNewPlanning = []
    if verbose:
        print("\t", shiftSwap)
    # create new swapped shifts
    newS = _prepare_after_swap_shifts(shiftSwap)
    if verbose:
        print("\t", newS)
    allShiftsForNewPlanning += newS
    # member 1 future setup:
    sM1TmpBefore = Shift.objects.filter(member=shiftSwap.shift.member, revision=baseRevision).filter(
        Q(date__lt=shiftSwap.shift_for_exchange.date) & Q(date__gt=(shiftSwap.shift_for_exchange.date + timedelta(days=-previewRangeDays)))
    )
    sM1TmpAfter = Shift.objects.filter(member=shiftSwap.shift.member, revision=baseRevision).filter(
        Q(date__gt=shiftSwap.shift_for_exchange.date) & Q(date__lt=(shiftSwap.shift_for_exchange.date + timedelta(days=previewRangeDays)))
    )
    if verbose:
        print("\t", sM1TmpBefore, sM1TmpAfter)
    for s in list(sM1TmpAfter) + list(sM1TmpBefore):
        if s.id in usedSlots:
            continue
        usedSlots.append(s.id)
        allShiftsForNewPlanning.append(s)
    sM2TmpBefore = Shift.objects.filter(member=shiftSwap.shift_for_exchange.member, revision=baseRevision).filter(
        Q(date__lt=shiftSwap.shift.date) & Q(date__gt=(shiftSwap.shift.date + timedelta(days=-previewRangeDays)))
    )
    sM2TmpAfter = Shift.objects.filter(member=shiftSwap.shift_for_exchange.member, revision=baseRevision).filter(
        Q(date__gt=shiftSwap.shift.date) & Q(date__lt=(shiftSwap.shift.date + timedelta(days=previewRangeDays)))
    )
    if verbose:
        print("\t", sM2TmpBefore, sM2TmpAfter)
    for s in list(sM2TmpAfter) + list(sM2TmpBefore):
        if s.id in usedSlots:
            continue
        usedSlots.append(s.id)
        allShiftsForNewPlanning.append(s)
    return allShiftsForNewPlanning


def _create_shift(
    shift: Shift, member: Member, slot: Slot = None, date=None, revision: Revision = None, is_active=True, post_comment=None, permanent=False, pre_comment=None
) -> Shift:
    """
    Creates a shift from a shift but with changed member and revision
    """
    # TODO check if swap should be permitted for: isActive? isChanged?
    s = Shift()
    s.member = member
    s.slot = shift.slot
    if slot is not None:
        s.slot = slot
    s.campaign = shift.campaign
    s.date = shift.date
    if date is not None:
        s.date = date
    s.shiftID = shift.shiftID
    s.is_active = is_active
    s.is_cancelled = False
    s.revision = shift.revision if revision is None else revision
    s.csv_upload_tag = shift.csv_upload_tag
    if pre_comment is not None:
        s.pre_comment = pre_comment
    if post_comment is not None:
        s.post_comment = post_comment
    if permanent:
        s.save()
    return s


def _prepare_after_swap_shifts(shiftExchangePair: ShiftExchangePair, permanent=False) -> list:
    s1 = _create_shift(shiftExchangePair.shift, shiftExchangePair.shift_for_exchange.member, permanent=permanent)
    s2 = _create_shift(shiftExchangePair.shift_for_exchange, shiftExchangePair.shift.member, permanent=permanent)
    return [s1, s2]


def _update_exchange_pair(oneSwap, revisionBackup):
    oneSwap.shift.revision = revisionBackup
    oneSwap.shift.is_active = False
    oneSwap.shift.save()
    oneSwap.shift_for_exchange.revision = revisionBackup
    oneSwap.shift_for_exchange.is_active = False
    oneSwap.shift_for_exchange.save()


def is_valid_for_hours_constraints(
    shiftExchange: ShiftExchange,
    member: Member,
    baseRevision: Revision,
) -> tuple:
    """
    returns tuple of boolean, and list of violated shifts
    """
    closeScheduleAfterUpdate = get_exchange_exchange_preview(shiftExchange, baseRevision)
    # FIXME consider ShiftExchange per only TWO members, then one can save calls to check validity for each member
    s1 = find_daily_rest_time_violation([x for x in closeScheduleAfterUpdate if x.member == member])
    is_daily_respected = len(s1) == 0
    s2 = find_weekly_rest_time_violation([x for x in closeScheduleAfterUpdate if x.member == member])
    is_weekly_respected = len(s2) == 0
    return is_daily_respected & is_weekly_respected, (s1, s2)


def is_valid_for_hours_constraints_from_market(sfm: ShiftForMarket, revision: Revision, member: Member) -> tuple:
    """
    :param sfm: ShiftForMarket shift on market
    :param revision: Revision to check (usually the current one)
    :param member: Member to check for usually the one that is to be swapped/being logged
    :return: tuple of (boolean, list of violated shifts/new working hours within demarcation period)

    Performs check for the following constraints:

    1. daily rest time
    2. weekly rest time
    3. maximum hours scheduled per cycle

    """
    shiftSwap = ShiftExchangePair(shift=sfm.shift, shift_for_exchange=_create_shift(sfm.shift, member))
    # print(sfm)
    new_target_shift = shiftSwap.shift_for_exchange

    a = _verifyForOneShiftSwap(shiftSwap, [shiftSwap.shift.id, new_target_shift.id], revision, verbose=False)
    s1 = find_daily_rest_time_violation([x for x in a if x.member == member])
    is_daily_respected = len(s1) == 0

    s2 = find_weekly_rest_time_violation([x for x in a if x.member == member])
    is_weekly_respected = len(s2) == 0

    working_h_with_added_shift, working_hours_old = _how_many_hours_with_new_shift_within_demarcation_period(member, new_target_shift, revision)
    is_demarcation_respected = working_h_with_added_shift < MAX_HOURS_SCHEDULED_PER_CYCLE

    return is_daily_respected & is_weekly_respected & is_demarcation_respected, (s1, s2, working_h_with_added_shift, working_hours_old)


def _how_many_hours_with_new_shift_within_demarcation_period(member, new_target_shift, revision):
    d_rng = find_current_demarcation_range(today=new_target_shift.start)
    current_equivalent_shift = Shift.objects.filter(member=member, date=new_target_shift.date, revision=revision, is_active=True).first()
    current_shift_duration = get_reportable_shift_duration_h(current_equivalent_shift)
    current_working_time = get_working_hours({"revision": revision, "is_active": True, "date__gte": d_rng[0], "date__lte": d_rng[1]}, [member], d_rng)
    new_shift_duration = get_reportable_shift_duration_h(new_target_shift)
    new_working_h_with_added_shift = current_working_time[0]["demarcation_period_hours"] + new_shift_duration - current_shift_duration
    return new_working_h_with_added_shift, current_working_time[0]["demarcation_period_hours"]


def perform_exchange_and_save_backup(shiftExchange: ShiftExchange, approver: Member, revisionBackup: Revision, verbose=False) -> list:
    """
    creates new shifts for the proposed slots, keeps the old ones in the revisionBackup,
    makes the shiftExchange as Done==True

    Warning: this call, changes the shifts!
    """
    if revisionBackup is None:
        raise ValueError("The backup revision needs to be provided to store the shift exchange!")
    if verbose:
        print("\t============================")
        print("\t----- SAVING EXCHANGE ------")
        print("\t============================")
        print(shiftExchange)
        print("Approved by ", approver)
        print("Backup rev to use ", revisionBackup)
    shiftsAfterSwap = []
    for oneSwap in shiftExchange.shifts.all():
        shifts = _prepare_after_swap_shifts(oneSwap, permanent=True)
        shiftsAfterSwap += shifts
        _update_exchange_pair(oneSwap, revisionBackup)

    shiftExchange.implemented = True
    shiftExchange.approver = approver
    shiftExchange.approved = timezone.localtime(timezone.now())
    shiftExchange.save()
    if verbose:
        print("\t============================")
    return shiftsAfterSwap


def perform_simplified_exchange_and_save_backup(
    shift: Shift, newMember: Member, requestor: Member, approver: Member, revisionBackup: Revision, exType="Normal"
) -> ShiftExchange:
    """Performs a simplified shift exchange when in the existing shift new member is created"""
    fakeShift = _create_shift(
        shift,
        newMember,
        revision=revisionBackup,
        pre_comment="FakeAndTemporary shift created when updating the change of Member",
        is_active=False,
        permanent=True,
    )
    sPair = ShiftExchangePair.objects.create(shift=shift, shift_for_exchange=fakeShift)
    sEx = ShiftExchange()
    sEx.requestor = requestor
    sEx.type = exType
    sEx.approver = approver
    sEx.backupRevision = revisionBackup
    sEx.requested = timezone.now()
    # FIXME consider actually performing the check. TO be seen how to 'avoid' constraints for the fake swap partner
    sEx.applicable = True
    sEx.tentative = False
    sEx.save()
    sEx.shifts.add(sPair)
    sEx.save()
    perform_exchange_and_save_backup(sEx, approver, revisionBackup=sEx.backupRevision, verbose=False)
    return sEx
