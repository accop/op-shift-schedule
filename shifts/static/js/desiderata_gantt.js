var dStart;
var dEnd;
const currentYear = new Date().getFullYear();

$(document).ready(function() {
    dStart = `${currentYear}-01-01`;
    dEnd = `${currentYear}-12-31`;

    $('#stat_desiderata_data_range_picker').daterangepicker({
        opens: 'left',
        locale: {
            firstDay : 1
        }
    });

    $('#stat_desiderata_data_range_picker').on('change', function() {
        var DateRangeStart = $('#stat_desiderata_data_range_picker').data('daterangepicker').startDate;
        dStart = DateRangeStart.format('YYYY-MM-DD');

        var DateRangeEnd = $('#stat_desiderata_data_range_picker').data('daterangepicker').endDate;
        dEnd = DateRangeEnd.format('YYYY-MM-DD');

        fill_gantt_plots();
    });

    fill_gantt_plots();
});

function fill_gantt_plots() {
    var teamId = $("team_id").data('id');

    var startDate = new Date(dStart);
    var endDate = new Date(dEnd);

    var xAxisMin = Date.UTC(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
    var xAxisMax = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());

    $.ajax({
        dataType: "json",
        method: "GET",
        url: $('#gantt-container').data('content_url'),
        data: { start: dStart, end: dEnd, team: teamId},
        success: function(dataJSON) {
            var roleColors = {
                'Operator': '#AFF359',
                'ShiftLeader': '#1338BE',
            };

            dataJSON.series.data.forEach(function(point) {
                point.color = roleColors[point.role] || '#000000';
            });

            Highcharts.ganttChart('gantt-container', {
                title: {
                    text: "Team Desiderata for overview"
                },
                xAxis: {
                    min: xAxisMin,
                    max: xAxisMax,
                    labels: {
                        formatter: function() {
                            var diffInMonths = (endDate.getFullYear() - startDate.getFullYear()) * 12 + endDate.getMonth() - startDate.getMonth() + 1;
                            if (diffInMonths < 6) {
                                return Highcharts.dateFormat('W %W', this.value);
                            } else {
                                return Highcharts.dateFormat('%b', this.value);
                            }
                        },
                    },
                },
                yAxis: dataJSON.yAxis,
                series: [{
                    name: dataJSON.series.Name,
                    data: dataJSON.series.data,
                    pointWidth: 30,
                }]
            });
        },
        error: function() {
            alert('There was an error while fetching stats!');
        }
    });
}
