$(document).ready(function() {
    $('#shiftControl_date_range_picker').daterangepicker({
        opens: 'left',
        locale: {
            firstDay : 1
        }
    });

    $('#shiftControl_date_range_picker').on('change', function() {
        $('#table_ShiftControl').DataTable().ajax.reload();
    });

    $('#table_ShiftControl').DataTable({
        ajax: {
            url: $('#table_ShiftControl').data('source'),
            data: function(d) {
                d.start = $('#shiftControl_date_range_picker').data('daterangepicker').startDate.format('YYYY-MM-DD');
                d.end = $('#shiftControl_date_range_picker').data('daterangepicker').endDate.format('YYYY-MM-DD');
                d.team = $("team_id").data('id');
            },
            dataSrc: 'data',
        },
        dom: 'P',
        bPaginate: false,
        paging: false,
        searchPanes: {
            initCollapsed: true
        },
        columns: [
            { data: 'date', title: 'Day' },
            { data: 'members', title: 'Crew' },
        ],
        autoWidth: false,
    });
});
