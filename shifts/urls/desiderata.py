from django.urls import path

from shifts.views import desiderata

urlpatterns = [
    path("user", desiderata.user, name="desiderata.user"),
    path("add", desiderata.add, name="desiderata.add"),
    path("edit", desiderata.edit, name="desiderata.edit"),
    path("delete", desiderata.delete, name="desiderata.delete"),
    path("get_user_desiderata", desiderata.get_user_desiderata, name="desiderata.get_user_desiderata"),
    path("team_view/<int:team_id>", desiderata.team_view, name="desiderata.team_view"),
    path("get_team_desiderata", desiderata.get_team_desiderata, name="desiderata.get_team_desiderata"),
    path("get_team_desiderata_export", desiderata.get_team_desiderata_export, name="desiderata.get_team_desiderata_export"),
    path("get_team_desiderata_team_view", desiderata.get_team_desiderata_non_rota_maker, name="desiderata.get_team_desiderata_non_rota_maker"),
    path("gantt_data", desiderata.gantt_data, name="desiderata.gantt_data"),
    path("get_team_working_hours", desiderata.get_team_working_hours, name="get_team_working_hours"),
]
