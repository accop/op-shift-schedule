from django.urls import path

from shifts.views import ajax as ajax_views

urlpatterns = [
    path("get_events", ajax_views.get_events, name="ajax.get_events"),
    path("get_events_csv", ajax_views.get_events_csv, name="ajax.get_events_csv"),
    path("get_user_events", ajax_views.get_user_events, name="ajax.get_user_events"),
    path("get_users_events", ajax_views.get_users_events, name="ajax.get_users_events"),
    path("get_team_events", ajax_views.get_team_events, name="ajax.get_team_events"),
    # path('get_user_future_events', ajax_views.get_user_future_events, name='ajax.get_user_future_events'),
    # path('get_team_events', ajax_views.get_team_events, name='ajax.get_team_events'),
    path("get_holidays", ajax_views.get_holidays, name="ajax.get_holidays"),
    path("get_assets", ajax_views.get_assets, name="ajax.get_assets"),
    path("get_hr_codes", ajax_views.get_hr_codes, name="ajax.get_hr_codes"),
    path("get_team_shiftControl", ajax_views.get_team_shiftControl, name="ajax.get_team_shiftControl"),
    path("get_shift_inconsistencies", ajax_views.get_shift_inconsistencies, name="ajax.get_shift_inconsistencies"),
    path("get_team_hr_codes", ajax_views.get_team_hr_codes, name="ajax.get_team_hr_codes"),
    path("get_working_hours", ajax_views.get_working_hours, name="ajax.get_working_hours"),
    path("get_team_breakdown", ajax_views.get_shift_breakdown, name="ajax.get_team_breakdown"),
    path("get_shifts_for_exchange", ajax_views.get_shifts_for_exchange, name="ajax.get_shifts_for_exchange"),
    path("get_team_shift_inconsistencies", ajax_views.get_team_shift_inconsistencies, name="ajax.get_team_shift_inconsistencies"),
    path("get_applicability_for_change", ajax_views.get_applicability_for_change, name="ajax.get_applicability_for_change"),
    path("get_stats", ajax_views.get_shift_stats, name="ajax.get_stats"),
    path("get_team_shiftswaps", ajax_views.get_team_shiftswaps, name="ajax.get_team_shiftswaps"),
    path("get_market_events", ajax_views.get_market_events, name="ajax.get_market_events"),
    path("check_market", ajax_views.get_market_validity, name="ajax.check_market"),
    path("search", ajax_views.search, name="ajax.search"),
]
