from django.db.models import Q
from shifter.settings import DEFAULT_SHIFT_SLOT
from shifts.models import DATE_FORMAT_FULL, Shift, SIMPLE_DATE
from shifts.hrcodes import HANDOVER_IN_HOURS, NWH_DAY_DURATION_IN_HOURS, VACATION_DAY_IN_HOURS, MAX_HOURS_SCHEDULED_PER_CYCLE
from shifts.hrcodes import find_current_demarcation_range
from datetime import timedelta
import datetime
import time


def get_hours_break(laterShift: Shift, earlierShift: Shift):
    breakTotal = laterShift.start - earlierShift.end
    return breakTotal.seconds // 3600 + breakTotal.days * 24


def find_daily_rest_time_violation(scheduled_shifts, minimum_rest_time_in_hours=11) -> list:
    """
    Returns the tuple of pairs of shift that violate the rule for the minimum break
    """
    toReturn = []
    ss = list(scheduled_shifts)
    ss.sort(key=lambda s: s.start)
    for earlier, later in zip(ss[:-1], ss[1:]):
        if DEFAULT_SHIFT_SLOT in later.slot.abbreviation:
            continue
        if later.start - earlier.end < timedelta(hours=minimum_rest_time_in_hours):
            toReturn.append((earlier, later))
    return toReturn


def find_weekly_rest_time_violation(scheduled_shifts, minimum_rest_time=36) -> list:
    """
    Returns the tuple of pairs of shift that violate the rule for the minimum break
    """
    streak = []
    toReturn = []
    ss = list(scheduled_shifts)
    ss.sort(key=lambda s: s.start)
    for i, one in enumerate(ss[:-1]):
        nextShift = ss[i + 1]
        if nextShift.start - one.end < timedelta(hours=minimum_rest_time):
            streak.append(one)
        else:
            streak = []
        if len(streak) >= 10:
            if nextShift.start - one.end < timedelta(hours=minimum_rest_time):
                streak.append(ss[i + 1])
                toReturn.append(streak)
                streak = []
            else:
                streak = []

    # IDEA nb1 find any consecutive days, count <7 and see what is the break to the next one

    return toReturn


def find_working_hours(scheduled_shifts, startDate=None, endDate=None) -> dict:
    tempDates = {}
    for one in scheduled_shifts:
        # print(one)
        if tempDates.get(one.date, None) is None:
            tempDates[one.date] = {"start": None, "end": None}
        dateIn = tempDates[one.date]
        if dateIn.get("start", None) is None:
            dateIn["start"] = one.slot.hour_start
            dateIn["end"] = one.slot.hour_end
        if dateIn.get("start") > one.slot.hour_start:
            dateIn["start"] = one.slot.hour_start
        if one.slot.hour_end > dateIn.get("end"):
            dateIn["end"] = one.slot.hour_end
        if one.slot.hour_end >= dateIn.get("end") and dateIn.get("start") > one.slot.hour_end:
            dateIn["start"] = datetime.time(0, 0, 0)
            # TODO fix the nights
        if dateIn.get("start") > one.slot.hour_end:
            dateIn["end"] = datetime.time(23, 59, 59)
    slots = []
    totalWorkingTimeInH = timedelta(hours=0)
    for k, v in tempDates.items():
        hourInADayStart = datetime.datetime.combine(k, v["start"])
        hourInADayEnd = datetime.datetime.combine(k, v["end"])
        slots.append((hourInADayStart.strftime(DATE_FORMAT_FULL), hourInADayEnd.strftime(DATE_FORMAT_FULL)))
        totalWorkingTimeInH += hourInADayEnd - hourInADayStart

    return {
        "startDate": startDate.strftime(DATE_FORMAT_FULL) if startDate is not None else None,
        "endDate": endDate.strftime(DATE_FORMAT_FULL) if endDate is not None else None,
        "totalWorkingH": totalWorkingTimeInH.days * 24 + totalWorkingTimeInH.seconds // 3600,
        "workingSlots": slots,
    }


def get_days_of_week(weekNb, year=2024) -> list:
    """
    Returns all days of given week in given year.
    Starting from Monday to Sunday
    """
    WEEK = weekNb - 1
    startdate = time.asctime(time.strptime("{} {} 0".format(year, WEEK), "%Y %W %w"))
    startdate = datetime.datetime.strptime(startdate, "%a %b %d %H:%M:%S %Y")
    dates = []
    for i in range(1, 8):
        day = startdate + datetime.timedelta(days=i)
        dates.append(day)
    return dates


def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
    return next_month - datetime.timedelta(days=next_month.day)


def get_working_hours(filterDict, members, range_of_current_cycle=None):
    """
    :param filterDict: shall include at minimum:  {"date__gte": x, "date__lte": y, "revision": z}
    :param range_of_current_cycle : tuple of two datetime objects
    :param members: members to count for

    Returns a dictionary of member to counted hours in a given time range. The following sums are assumed:

    - Shift slot (with handover) == 8.25h

    - NWH shift (office) == 7.8h

    - Shift slot OR NWH in vacation == 8h
    """
    team_data = []
    if range_of_current_cycle is None:
        range_of_current_cycle = find_current_demarcation_range()
    for member in members:
        filterDict["member"] = member
        scheduled_shifts = Shift.objects.filter(**filterDict).filter(Q(is_active=True) | Q(is_vacation=True)).order_by("-date")
        total_working_hours = 0
        total_working_hours_with_NWH = 0
        demarcation_working_hours = 0
        demarcation_working_days = 0
        for shift in scheduled_shifts:
            duration = get_reportable_shift_duration_h(shift)
            duration_with_NWH = get_reportable_shift_duration_h(shift)
            if "NWH" in shift.slot.abbreviation or shift.is_vacation:
                duration = 0

            if range_of_current_cycle[0].date() <= shift.date < range_of_current_cycle[1].date():
                demarcation_working_days += 1
                demarcation_working_hours += duration_with_NWH if shift.role is not None else duration
            if shift.role is not None:
                total_working_hours_with_NWH += duration_with_NWH
            else:
                total_working_hours += duration

        team_data.append(
            {
                "member_name": member.get_full_name(),
                "total_working_hours_NWH": round(total_working_hours_with_NWH, 2),
                "total_working_hours": round(total_working_hours, 2),
                "demarcation_period_hours": round(demarcation_working_hours, 2),
                "demarcation_period_start": range_of_current_cycle[0].strftime(SIMPLE_DATE),
                "demarcation_period_end": range_of_current_cycle[1].strftime(SIMPLE_DATE),
                "demarcation_working_days": demarcation_working_days,
                "demarcation_max_allowed_working_hours": MAX_HOURS_SCHEDULED_PER_CYCLE,
            }
        )
    return team_data


def get_reportable_shift_duration_h(shift: Shift) -> float:
    """
    Returns the duration of the shift in hours, considering the shift slot and the handover time.
    """
    if shift is None:
        return 0
    duration = shift.duration.total_seconds() / 3600
    if shift.slot.with_handover:
        duration += HANDOVER_IN_HOURS
    if "NWH" in shift.slot.abbreviation:
        duration = NWH_DAY_DURATION_IN_HOURS
    if shift.is_vacation:
        duration = VACATION_DAY_IN_HOURS
    return duration
