from datetime import date, timedelta, time, datetime
from typing import Tuple
from shifts.models import Shift, SIMPLE_DATE

NWH = "NWH"
OB4 = "OB4"
OB4_WEIGHT = 1 / 150
OB3 = "OB3"
OB3_WEIGHT = 1 / 300
OB2 = "OB2"
OB2_WEIGHT = 1 / 400
OB1 = "OB1"
OB1_WEIGHT = 1 / 600
LC2 = "LC2"
LC1 = "LC1"
ELC = "ELC"
BTP_OR_BTB = "BTPorBTB"
OTT_OR_OTW = "OT"

OB_CODES_TIMING = {
    # KEEP 'GRAVITY' ORDER
    NWH: (time(hour=7, minute=00, second=00), time(hour=17, minute=59, second=59)),  # days
    OB1: (time(hour=18, minute=00, second=00), time(hour=23, minute=59, second=59)),  # evenings
    OB2: (time(hour=0, minute=00, second=00), time(hour=7, minute=00, second=00)),  # nights
}

SATURDAY = 5
SUNDAY = 6
HANDOVER_IN_HOURS = 0.25
NWH_DAY_DURATION_IN_HOURS = 7.8
VACATION_DAY_IN_HOURS = 8.0
REDUCED_DAY_REDUCTION = 3.0
VALUE_OF_FULL_DAY_IN_HOURS = 8.0

# TODO consider moving these in the DB configurable fields
SHIFT_ROTATION_START = datetime(2024, 11, 4, 6, 29)
SHIFT_ROTATION_CYCLE_LENGTH_DAYS = timedelta(days=49)
MAX_HOURS_SCHEDULED_PER_CYCLE = 243.5

# The ones that are counted as OB3 (as weekends)
# TODO think of importing it as external table (not need to re-release it)
# TODO consider https://pypi.org/project/holidays/ when Sweden is included (not in Nov 2021)
red_days = [
    date(2021, 11, 6),
    # https://confluence.esss.lu.se/pages/viewpage.action?spaceKey=HR&title=Public+Holidays+and+additional+days+off+%282022%29+Sweden
    date(2022, 1, 6),
    date(2022, 1, 7),
    date(2022, 5, 26),
    date(2022, 5, 27),
    date(2022, 6, 6),
    # https://confluence.esss.lu.se/pages/viewpage.action?spaceKey=HR&title=Public+holidays+and+additional+days+off+%282023%29+Sweden
    date(2023, 5, 1),
    date(2023, 5, 18),
    date(2023, 5, 19),
    date(2023, 6, 5),
    date(2023, 6, 6),
    # https://confluence.esss.lu.se/display/HR/Public+holidays+and+additional+days+off+%282024%29+Sweden
    date(2024, 5, 1),
    date(2024, 5, 9),
    date(2024, 5, 10),
    date(2024, 6, 6),
    date(2024, 6, 7),
    # https://confluence.ess.eu/display/HR/Public+holidays+and+additional+days+off+%282025%29+Sweden
    date(2025, 1, 6),
    date(2025, 5, 1),
    date(2025, 5, 29),
    date(2025, 6, 6),
]

#
# These, with the new 2024-09-13 agrement will be treaded special bank/pay
reduced_days = [
    date(2021, 1, 5),
    date(2021, 4, 1),
    date(2021, 4, 30),
    date(2021, 11, 5),
    date(2022, 1, 5),
    date(2022, 4, 14),
    date(2022, 11, 4),
    # these days are reduced by 3h, and start the 'WE' OB code as of 14:00
    # https://confluence.esss.lu.se/pages/viewpage.action?spaceKey=HR&title=Public+holidays+and+additional+days+off+%282023%29+Sweden
    date(2023, 1, 5),
    date(2023, 4, 6),
    date(2023, 11, 3),
    # https://confluence.esss.lu.se/display/HR/Public+holidays+and+additional+days+off+%282024%29+Sweden
    date(2024, 1, 5),
    date(2024, 3, 28),
    date(2024, 4, 30),
    date(2024, 11, 1),
    # https://confluence.ess.eu/display/HR/Public+holidays+and+additional+days+off+%282025%29+Sweden
    # 3h
    date(2025, 4, 17),
    date(2025, 4, 30),
    date(2025, 10, 31),
]

bridge_days = [
    # 8h:
    date(2024, 12, 27),
    date(2025, 5, 2),
    date(2025, 5, 30),
]

# The ones that are counted as OB4
# From 18:00 on Maundy Thursday and from 07:00 on Whitsun Eve, Midsummer Eve,
# Christmas Eve and New Year's Eve until midnight before the first weekday after the holiday.
# the first group is to cover the 'special' earlier 18:00 in Maundy Thu
public_holidays_special_exception = [(date(2022, 4, 14), time(18, 0, 0)), (date(2023, 4, 6), time(18, 0, 0))]
public_holidays_special = [
    date(2021, 6, 25),
    date(2021, 12, 24),
    date(2021, 12, 25),
    date(2021, 12, 26),
    date(2021, 12, 31),
    # https://confluence.esss.lu.se/pages/viewpage.action?spaceKey=HR&title=Public+Holidays+and+additional+days+off+%282022%29+Sweden
    date(2022, 1, 1),
    date(2022, 4, 15),
    date(2022, 4, 16),
    date(2022, 4, 17),
    date(2022, 4, 18),
    date(2022, 6, 24),
    date(2022, 6, 25),
    date(2022, 12, 24),
    date(2022, 12, 25),
    date(2022, 12, 26),
    date(2022, 12, 31),
    # https://confluence.esss.lu.se/pages/viewpage.action?spaceKey=HR&title=Public+holidays+and+additional+days+off+%282023%29+Sweden
    date(2023, 1, 1),
    # date(2023, 4, 9), reduced 3
    date(2023, 4, 7),
    date(2023, 4, 8),
    date(2023, 4, 9),
    date(2023, 4, 10),
    date(2023, 6, 23),
    date(2023, 12, 25),
    date(2023, 12, 26),
    # https://confluence.esss.lu.se/display/HR/Public+holidays+and+additional+days+off+%282024%29+Sweden
    date(2024, 1, 1),
    date(2024, 3, 29),
    date(2024, 4, 1),
    date(2024, 6, 21),
    date(2024, 12, 24),
    date(2024, 12, 25),
    date(2024, 12, 26),
    date(2024, 12, 31),
    # https://confluence.ess.eu/display/HR/Public+holidays+and+additional+days+off+%282025%29+Sweden
    date(2025, 1, 1),
    date(2025, 4, 18),
    date(2025, 4, 19),
    date(2025, 4, 20),
    date(2025, 4, 21),
    date(2025, 6, 20),
    date(2025, 12, 24),
    date(2025, 12, 25),
    date(2025, 12, 26),
    date(2025, 12, 31),
]


def find_current_demarcation_range(today=datetime.today(), start=SHIFT_ROTATION_START, cycle_length=SHIFT_ROTATION_CYCLE_LENGTH_DAYS) -> tuple:
    """
    :returns: a tuple of start and end of the current demarcation period
    """
    last_demarcation_start = start
    while last_demarcation_start < today - cycle_length:
        last_demarcation_start += cycle_length
    return last_demarcation_start, last_demarcation_start + cycle_length


def get_public_holidays(fmt=None):
    ph = public_holidays_special + red_days + bridge_days
    ph.sort()
    if fmt is None:
        return [d for d in ph]
    return [d.strftime(format=fmt) for d in ph]


def count_total(counts):
    countsReturn = {
        OB1: 0,
        OB2: 0,
        OB3: 0,
        OB4: 0,
        NWH: 0,
        ELC: 0,
        LC1: 0,
        LC2: 0,
        OTT_OR_OTW: 0,
        BTP_OR_BTB: 0,
    }
    for oneDay in counts.keys():
        for oneCode in countsReturn.keys():
            countsReturn[oneCode] += counts.get(oneDay)[oneCode]
    return countsReturn


def get_date_code_counts(shifts):
    result = {}
    corrections = []
    for shift in shifts:
        dayString = shift.start.date().strftime(SIMPLE_DATE)
        count, correctionCount = get_code_counts(shift)
        if len(correctionCount):
            corrections.append(correctionCount)
        result[dayString] = count

    for correction in corrections:
        for dateStr, correctionCodeDictionary in correction.items():
            for code, value in correctionCodeDictionary.items():
                result[dateStr][code] = value
    return result


def _is_the_following_day_a_WE_or_holiday(shiftDate: date):
    nextDay = shiftDate + timedelta(days=1)
    if nextDay.weekday() in [SATURDAY, SUNDAY]:
        return True
    if nextDay in public_holidays_special or nextDay in red_days or nextDay in bridge_days:
        return True
    return False


def _is_over_night(shift: Shift):
    return shift.start.date() != shift.end.date()


def get_code_counts(shift: Shift, granulation=900, returnFactor=4, handoverTime=HANDOVER_IN_HOURS, verbose=False) -> Tuple[dict, dict]:
    """
    granulation 15min == 900seconds
    return => 4 x 15min, in hours
    handover 0.25h == 15min

    returns first is the shift count Dictionary, second is the optional dictionary with date to correct
    """
    if verbose:
        print("========================================\n =======< {} {} {} >=========".format(shift.date, shift.date.strftime("%A"), shift.slot))
    counts = {
        OB1: 0,
        OB2: 0,
        OB3: 0,
        OB4: 0,
        NWH: 0,
        ELC: 0,
        LC1: 0,
        LC2: 0,
        OTT_OR_OTW: 0,
        BTP_OR_BTB: 0,
    }
    correctionCounts = {}
    duration = shift.end - shift.start

    if verbose:
        print("Shift total duration: ", duration)
        print("Shift applicable with handover {} with {}h \n ".format(shift.slot.with_handover, handoverTime))
    notAWEOrHoliday = True

    if shift.changed_on_date:
        correctionCounts[shift.changed_on_date.strftime(SIMPLE_DATE)] = {ELC: 1}
        if shift.is_changed:
            counts[LC1] = 1
        if shift.is_changed_urgent:
            counts[LC2] = 1
        if shift.is_changed_offday:
            counts[OTT_OR_OTW] = 8.25
            notAWEOrHoliday = False

    # OB4 special holidays: Easter, Xmas, New Years and Midsommar
    if shift.date in public_holidays_special and not _is_the_following_day_a_WE_or_holiday(shift.date) and _is_over_night(shift):
        _countTimeWithinDates(shift, OB4, counts, dates=public_holidays_special, handoverTime=0.0)
        _countTimeForCode(shift, OB2, counts, handoverTime=handoverTime)
        notAWEOrHoliday = False
    if shift.date in public_holidays_special and notAWEOrHoliday:
        _countTimeWithinDates(shift, OB4, counts, dates=public_holidays_special, handoverTime=handoverTime)
        notAWEOrHoliday = False
    if verbose:
        print("public holiday spec: ", counts)

    # OB3 - regular red days
    if shift.date in red_days and not _is_the_following_day_a_WE_or_holiday(shift.date) and _is_over_night(shift):
        _countTimeWithinDates(shift, OB3, counts, dates=red_days, handoverTime=0.0)
        _countTimeForCode(shift, OB2, counts, handoverTime=handoverTime)
        notAWEOrHoliday = False
    if shift.date in red_days and notAWEOrHoliday:
        _countTimeWithinDates(shift, OB3, counts, dates=red_days, handoverTime=handoverTime)
        notAWEOrHoliday = False
    if verbose:
        print("Red_days OB3: ", counts)
    # OB3 - weekends
    if shift.start.weekday() == SUNDAY and notAWEOrHoliday and _is_over_night(shift):
        _countTimeWithinWE(shift, OB3, counts, handoverTime=0.0)
        _countTimeForCode(shift, OB2, counts, handoverTime=handoverTime)
        notAWEOrHoliday = False
    if shift.start.weekday() >= SATURDAY and notAWEOrHoliday:
        _countTimeWithinWE(shift, OB3, counts, handoverTime=handoverTime)
        notAWEOrHoliday = False
    if verbose:
        print("Weekends OB3: ", counts)

    if shift.date in reduced_days:
        counts[BTP_OR_BTB] = REDUCED_DAY_REDUCTION + _valueOfHandover(shift, handoverTime)

    if shift.date in bridge_days:
        counts[BTP_OR_BTB] = VALUE_OF_FULL_DAY_IN_HOURS + _valueOfHandover(shift, handoverTime)

    if notAWEOrHoliday:
        _countTimeForCode(shift, NWH, counts, handoverTime=0)
        _countTimeForCode(shift, OB1, counts, handoverTime=0)
        _countTimeForCode(shift, OB2, counts, handoverTime=0)
        _add_handover(shift, counts, handoverTime=handoverTime)

    if verbose:
        print(counts)
        print("---------------\n")
    return counts, correctionCounts


def _countTimeWithinDates(shift, code, counts, dates=None, stepMinutes=15, handoverTime=HANDOVER_IN_HOURS):
    if NWH in shift.slot.abbreviation:
        counts[code] = NWH_DAY_DURATION_IN_HOURS
        return None
    dateToCheck = shift.start
    while dateToCheck < shift.end and dateToCheck.date() in dates:
        if counts[code] < VALUE_OF_FULL_DAY_IN_HOURS:
            counts[code] += 0.25
        dateToCheck += timedelta(minutes=stepMinutes)
    counts[code] += _valueOfHandover(shift, handoverTime)


def _countTimeWithinWE(shift, code, counts, stepMinutes=15, handoverTime=HANDOVER_IN_HOURS):
    if NWH in shift.slot.abbreviation:
        counts[code] = NWH_DAY_DURATION_IN_HOURS
        return None
    dateToCheck = shift.start
    while dateToCheck < shift.end and dateToCheck.weekday() in [SATURDAY, SUNDAY]:
        if counts[code] < VALUE_OF_FULL_DAY_IN_HOURS:
            counts[code] += 0.25
        dateToCheck += timedelta(minutes=stepMinutes)
    counts[code] += _valueOfHandover(shift, handoverTime)


def _countTimeForCode(shift, code, counts, stepMinutes=15, handoverTime=HANDOVER_IN_HOURS):
    dateToCheck = shift.start
    if NWH in shift.slot.abbreviation:
        counts[NWH] = NWH_DAY_DURATION_IN_HOURS
        return None
    while dateToCheck < shift.end:
        if OB_CODES_TIMING[code][0] < (dateToCheck + timedelta(seconds=30)).time() < OB_CODES_TIMING[code][1]:
            if counts[code] < VALUE_OF_FULL_DAY_IN_HOURS:
                counts[code] += 0.25
        dateToCheck += timedelta(minutes=stepMinutes)
    _add_handover(shift, counts, handoverTime=handoverTime)


def _add_handover(shift, counts, handoverTime):
    if not shift.slot.with_handover:
        return None
    codeToBumpForHandover = None
    for oneCode in list(OB_CODES_TIMING.keys())[::-1]:  # reverse order of the defined codes
        if shift.slot.hour_end < OB_CODES_TIMING[oneCode][1]:
            codeToBumpForHandover = oneCode
            if shift.slot.hour_end < shift.slot.hour_start:
                break
    if codeToBumpForHandover is not None:
        counts[codeToBumpForHandover] += _valueOfHandover(shift, handoverTime)


def _valueOfHandover(shift, handoverTime):
    return handoverTime if shift.slot.with_handover else 0
