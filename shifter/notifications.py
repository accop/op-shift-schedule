"""
Prototype of the shifter-centralised notification implementation and classes.
"""

from abc import ABC, abstractmethod

from django.template.loader import render_to_string
from shifts.models import Shift, Revision, Member, ShiftExchange, ShiftForMarket
from studies.models import StudyRequest
from django.core.mail import send_mail

from notifications.signals import notify

from shifter.settings import DEFAULT_EMAIL_OPS

NOTIFICATIONS_CODE_SHIFT = 10
NOTIFICATION_CODE_REVISIONS = 20
NOTIFICATION_CODE_STUDIES = 30
NOTIFICATION_CODE_ERROR = -1


class NotificationService(ABC):
    """
    General notification system that takes an arbitrary event and broadcast by the means provided by the implementation.
    Supported events:
    - tuple of Shift - sent in case a modification is applied, i.e. two exchanged shifts,
    - StudyRequest - sent to Study Leader and Collaborators of the SR,
    - Revision - sent when new version is available with 'ready_for_preview==True',
    """

    #  TODO fix the url to be dynamic. There is no request context at this moment (or any other)
    def __init__(self, name, domain="https://shifter.esss.lu.se"):
        self._name = name
        self.default_domain = domain
        print("==>> Initialising ", name)

    @abstractmethod
    def _triggerShiftNotification(self, event, **kwargs):
        """
        triggers the notification on Shift/Collections of Shift/ShiftExchanges
        """
        pass

    @abstractmethod
    def _triggerRevisionNotification(self, event):
        """
        triggers the notification on Revision
        """
        pass

    @abstractmethod
    def _triggerStudyRequestNotification(self, event):
        """
        triggers the notification on StudyRequest
        """
        pass

    @abstractmethod
    def _triggerNotificationForMarket(self, event):
        """
        triggers the notification on StudyRequest
        """
        pass

    def notify(self, event, **kwargs) -> int:
        if isinstance(event, (Shift, NewShiftsUpload, ShiftExchange)):
            return self._triggerShiftNotification(event, **kwargs)
        elif isinstance(event, Revision):
            if event.ready_for_preview:
                return self._triggerRevisionNotification(event)
        elif isinstance(event, StudyRequest):
            return self._triggerStudyRequestNotification(event)
        elif isinstance(event, ShiftForMarket):
            return self._triggerNotificationForMarket(event)
        else:
            print("Do not know how to notify for ", event)
            return NOTIFICATION_CODE_ERROR


class NewShiftsUpload:
    def __init__(self, revision, campaign, affectedMembers, rotaMaker, startEnd, revisionFrom):
        self.revision = revision
        self.members = affectedMembers
        self.campaign = campaign
        self.rotaMaker = rotaMaker
        self.start = startEnd[0]
        self.end = startEnd[1]
        self.revisionFrom = revisionFrom


class DummyNotifier(NotificationService):
    """Mainly for test class"""

    def _triggerNotificationForMarket(self, event):
        pass

    def __init__(self, name):
        super().__init__(name=name)

    def _triggerShiftNotification(self, event, **kwargs):
        print("Something new with the following: ", event)
        return NOTIFICATIONS_CODE_SHIFT

    def _triggerRevisionNotification(self, revision):
        print("New planning is available in your 'My shifts' space. \n\n Search for {}".format(revision))
        return NOTIFICATION_CODE_REVISIONS

    def _triggerStudyRequestNotification(self, study):
        notify.send(study.member, recipient=study.member, verb="You have created the study {}".format(study.title))
        for one in study.collaborators.all():
            notify.send(one, recipient=one, verb="You have been added to the study {}".format(study.title))
        print("{}: NEW study requested {}".format(self._name, study))
        return NOTIFICATION_CODE_STUDIES


class EmailNotifier(NotificationService):
    """Simple Email creator, notifier and sender, using native Django send_mail and django-notification-hq"""

    DEFAULT_NO_REPLY = "noreply@ess.eu"

    STATUS_CONFIG = {
        "cancelled": {
            "subject": "Shift was cancelled",
            "template": "email_notifications/shift_cancelled_email.html",
            "notify_message": "Your shift was cancelled.",
            "return_code": NOTIFICATIONS_CODE_SHIFT,
        },
        "reactivated": {
            "subject": "Shift reactivated",
            "template": "email_notifications/shift_reactivation_email.html",
            "notify_message": "Your shift has been reactivated.",
            "return_code": NOTIFICATIONS_CODE_SHIFT,
        },
        "update": {
            "subject": "Shift updated",
            "template": "email_notifications/shift_updated_email.html",
            "notify_message": "Your shift has been updated!",
            "return_code": NOTIFICATIONS_CODE_SHIFT,
        },
    }

    def _notify_internal_and_external(self, actor, emailSubject, emailBody, affectedMembers, affectedMembersEmail, target=None):
        # sends the Django notifications
        notify.send(actor, recipient=affectedMembers, target=target, verb=emailSubject, description=emailBody, emailed=True)
        # sends the email
        # FIXME maybe replace with the cron/job to sent 'unread' notifications (once a day)
        send_mail(
            emailSubject,
            emailBody,
            self.DEFAULT_NO_REPLY,
            [one.email for one in affectedMembersEmail],
            fail_silently=False,
        )

    def _triggerShiftNotification(self, event, **kwargs):
        if isinstance(event, Shift):
            pass

        if isinstance(event, NewShiftsUpload) and event.revision.valid:  # not valid revisions go with another notify
            emailSubject = "[shifter] Schedule in {} updated".format(event.revision)
            emailBody = render_to_string(
                "email_notifications/shiftsupload_email.html",
                {
                    "campaign": event.campaign,
                    "revision": event.revision,
                    "importRevision": event.revisionFrom,
                    "rotaMaker": event.rotaMaker,
                    "import_start": event.start,
                    "import_end": event.end,
                    "domain": self.default_domain,
                    "op_email": DEFAULT_EMAIL_OPS,
                },
            )

            self._notify_internal_and_external(
                actor=event.rotaMaker,
                target=None,
                emailSubject=emailSubject,
                emailBody=emailBody,
                affectedMembers=event.members,
                affectedMembersEmail=[one for one in event.members if one.notification_shifts],
            )

        if isinstance(event, Shift) and kwargs.get("status", None) in EmailNotifier.STATUS_CONFIG:
            config = EmailNotifier.STATUS_CONFIG[kwargs.get("status")]
            subject = config["subject"]

            email_body = render_to_string(
                config["template"],
                {
                    "member": event.member,
                    "shift": event,
                    "sExId": event.id,
                    "domain": self.default_domain,
                    "op_email": DEFAULT_EMAIL_OPS,
                    "old_shift": kwargs.get("old_shift", None),
                },
            )

            affectedMembers = [event.member]
            self._notify_internal_and_external(
                actor=event.member,
                target=event,
                emailSubject=subject,
                emailBody=email_body,
                affectedMembers=affectedMembers,
                affectedMembersEmail=[one for one in affectedMembers if one.notification_shifts],
            )

            return config["return_code"]

        if isinstance(event, ShiftExchange):
            sExId = event.id
            affectedMembers = [event.approver, event.requestor]
            for one in event.shifts.all():
                affectedMembers.append(one.shift.member)
                affectedMembers.append(one.shift_for_exchange.member)
            affectedMembers = list(set(affectedMembers))

            if event.tentative and event.applicable:
                emailSubject = "[shifter] Shift Exchange (#{}) REQUEST".format(sExId)
                emailBody = render_to_string(
                    "email_notifications/shiftexchange_email.html",
                    {
                        "approve": event.approver,
                        "requestor": event.requestor,
                        "shifts": event.shifts.all(),
                        "sExId": sExId,
                        "domain": self.default_domain,
                        "op_email": DEFAULT_EMAIL_OPS,
                    },
                )

                self._notify_internal_and_external(
                    actor=event.approver,
                    target=event,
                    emailSubject=emailSubject,
                    emailBody=emailBody,
                    affectedMembers=affectedMembers,
                    affectedMembersEmail=[one for one in affectedMembers if one.notification_shifts],
                )

        if isinstance(event, ShiftExchange):
            sExId = event.id
            affectedMembers = [event.approver, event.requestor]
            for one in event.shifts.all():
                affectedMembers.append(one.shift.member)
                affectedMembers.append(one.shift_for_exchange.member)
            affectedMembers = list(set(affectedMembers))

            if event.tentative and event.applicable:
                emailSubject = "[shifter] Shift Exchange (#{}) REQUEST".format(sExId)
                emailBody = render_to_string(
                    "email_notifications/shiftexchange_email.html",
                    {
                        "approve": event.approver,
                        "requestor": event.requestor,
                        "shifts": event.shifts.all(),
                        "sExId": sExId,
                        "domain": self.default_domain,
                        "op_email": DEFAULT_EMAIL_OPS,
                    },
                )

                self._notify_internal_and_external(
                    actor=event.approver,
                    target=event,
                    emailSubject=emailSubject,
                    emailBody=emailBody,
                    affectedMembers=affectedMembers,
                    affectedMembersEmail=[one for one in affectedMembers if isinstance(one, Member) and one.notification_shifts],
                )
            elif event.applicable:
                emailSubject = "[shifter] Shift Exchange (#{}) CONFIRMATION".format(sExId)
                emailBody = render_to_string(
                    "email_notifications/shiftexchangeconfirmation_email.html",
                    {
                        "approve": event.approver,
                        "requestor": event.requestor,
                        "shifts": event.shifts.all(),
                        "sExId": sExId,
                        "domain": self.default_domain,
                        "op_email": DEFAULT_EMAIL_OPS,
                    },
                )
                self._notify_internal_and_external(
                    actor=event.approver,
                    target=event,
                    emailSubject=emailSubject,
                    emailBody=emailBody,
                    affectedMembers=affectedMembers,
                    affectedMembersEmail=[one for one in affectedMembers if isinstance(one, Member) and one.notification_shifts],
                )
                # ToDo fix hardcoding "role = 1"
                team_members_with_role = Member.objects.filter(team=event.requestor.team, role=1, is_active=True)  # 1 = LineManager

                for member in team_members_with_role:
                    if member.email:
                        manager_email_subject = "[shifter] Shift Exchange (#{}) Team Notification".format(sExId)
                        manager_email_body = render_to_string(
                            "email_notifications/shiftexchange_manager_notification.html",
                            {
                                "approve": event.approver,
                                "requestor": event.requestor,
                                "shifts": event.shifts.all(),
                                "sExId": sExId,
                                "domain": self.default_domain,
                                "op_email": DEFAULT_EMAIL_OPS,
                            },
                        )

                        send_mail(
                            manager_email_subject,
                            manager_email_body,
                            self.DEFAULT_NO_REPLY,
                            [member.email],
                            fail_silently=False,
                            html_message=manager_email_body,
                        )

            return NOTIFICATIONS_CODE_SHIFT

        return NOTIFICATION_CODE_ERROR

    def _triggerNotificationForMarket(self, shiftForMarket: ShiftForMarket):
        emailSubject = "[shifter] NEW shift on market"
        emailBody = render_to_string(
            "email_notifications/shift_market_email.html",
            {"shiftForMarket": shiftForMarket, "domain": self.default_domain, "op_email": DEFAULT_EMAIL_OPS},
        )
        m = Member.objects.filter(role=shiftForMarket.shift.member.role, is_active=True)
        affectedMembers = list(m)

        self._notify_internal_and_external(
            actor=shiftForMarket.offerer,
            target=shiftForMarket,
            emailSubject=emailSubject,
            emailBody=emailBody,
            affectedMembers=affectedMembers,
            affectedMembersEmail=[one for one in affectedMembers if one.notification_shifts],
        )

    def _triggerRevisionNotification(self, revision):
        emailSubject = "[shifter] NEW planning ready for the preview"
        emailBody = render_to_string(
            "email_notifications/shiftsnewrevision_email.html",
            {"revision": revision, "domain": self.default_domain, "op_email": DEFAULT_EMAIL_OPS},
        )
        affectedMembers = list(set([one.member for one in Shift.objects.filter(revision=revision)]))
        #  TODO Fix AnonymousUser. Seems like there is no good option to pass the actual user
        self._notify_internal_and_external(
            actor=Member.objects.get(username="AnonymousUser"),
            target=revision,
            emailSubject=emailSubject,
            emailBody=emailBody,
            affectedMembers=affectedMembers,
            affectedMembersEmail=[one for one in affectedMembers if one.notification_shifts],
        )

    def _triggerStudyRequestNotification(self, study):
        emailSubject = "[shifter STUDY][{}] {}".format(study.state_full(), study.title)
        emailBody = render_to_string(
            "email_notifications/studyrequest_email.html",
            {"study": study, "studyState": study.state_full(), "col": study.collaborators.all(), "domain": self.default_domain, "op_email": DEFAULT_EMAIL_OPS},
        )
        self._notify_internal_and_external(
            actor=study.member,
            target=study,
            emailSubject=emailSubject,
            emailBody=emailBody,
            affectedMembers=study.all_involved(),
            affectedMembersEmail=[one for one in study.all_involved() if one.notification_studies],
        )


notificationService = EmailNotifier("[SHIFTER main Notifier]")
