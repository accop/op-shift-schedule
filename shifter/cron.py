from django_cron import CronJobBase, Schedule
from shifts.models import *
from shifts.views.ajax import _create_shift_data, parse_date, _format_csv_data
import csv


class DailyDumpOfYearlyScheduleIntoCSV(CronJobBase):
    """
    Selects all ACTIVE shifts from the last valid revision (BASE)
    for all members, between now.year Jan 1st, and the last scheduled shift.
    """

    RUN_EVERY_MINS = 24 * 60

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = "shifter.DailyDumpOfYearlyScheduleIntoCSV"

    def do(self):
        print("============")
        print("=> Yearly CSV dump every {} minutes".format(self.RUN_EVERY_MINS))
        print("============")
        validRevision = Revision.objects.filter(valid=True).order_by("-number").first()
        now = datetime.datetime.now()
        start = datetime.date(now.year, 1, 1)
        lastShift = Shift.objects.filter(is_active=True, revision=validRevision).order_by("-date").first()
        end = lastShift.date
        start = parse_date(start.strftime(SIMPLE_DATE), with_time=True).replace(hour=0, minute=0, second=0, microsecond=0)
        end = parse_date(end.strftime(SIMPLE_DATE), with_time=True).replace(hour=23, minute=59, second=59, microsecond=999999)
        filter_dic = {
            "date__gte": start,
            "date__lte": end,
            "revision": validRevision,
            "is_active": True,
        }
        scheduled_shifts = Shift.objects.filter(**filter_dic).order_by("date", "slot__hour_start", "member__role__priority")
        shift_data = _create_shift_data(scheduled_shifts, start, end)
        headers, data = _format_csv_data(shift_data, start, end)
        self.write_file(data, headers, "Dump_{}_by_CRON_JOB__{}.csv".format(now.year, now.strftime(SIMPLE_DATE)))
        print("============")

    def write_file(self, data, headers, fileName):
        with open(fileName, "w", newline="") as csvfile:
            writer = csv.writer(csvfile, delimiter=",")
            writer.writerow(headers)
            for row in data:
                writer.writerow(row)


class DailyNotificationBatchEmail(CronJobBase):
    RUN_EVERY_MINS = 24 * 60

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = "shifter.DailyNotificationBatchEmail"

    def do(self):
        #  TODO implement new model change to aggregate and batch email the "unread" notifications.
        pass
